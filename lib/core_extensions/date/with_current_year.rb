module CoreExtensions
  module Date
    module WithCurrentYear
      def with_current_year
        years_ago = self.class.today.year - year
        next_year(years_ago)
      end
    end
  end
end
