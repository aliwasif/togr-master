module CustomTokenResponse
  def body
    { data: { access: super }, status: Rack::Utils.status_code(:created) }
  end
end
