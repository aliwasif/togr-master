namespace :vendor do
  desc "Populate Pulse Experiential Items in DB"
  task populate_pulse_experiential_items: :environment do
    puts 'Populating Pulse Experiential items in DB...'
    Vendors::PulseExperiential::PulseExperientialService.new.populate_items
  end
end
