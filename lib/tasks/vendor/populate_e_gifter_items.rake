namespace :vendor do
  desc "Populate eGifter Items in DB"
  task populate_e_gifter_items: :environment do
    puts 'Populating eGifter items in DB...'
    Vendors::EGifter::EGifterService.new(sub_order: nil).populate_items
  end
end
