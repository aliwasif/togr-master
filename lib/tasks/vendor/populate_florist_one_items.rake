namespace :vendor do
  desc "Populate FloristOne Items in DB"
  task populate_florist_one_items: :environment do
    puts 'Populating FloristOne items in DB...'
    Vendors::FloristOne::FloristOneService.new(sub_order:nil).populate_items
  end
end
