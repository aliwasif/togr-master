namespace :notifications do
  desc "Truncate notifications older than 6 months"
  task truncate: :environment do
    puts 'Truncating notifications older than 6 months...'
    Notification.where("date < ?", 6.months.ago.to_date.to_s).in_batches.destroy_all
  end
end
