desc "Populate eGreetingCards in DB"
task populate_greeting_cards: :environment do
  puts 'Populating Greeting Cards in DB...'
  Artist.destroy_all
  artists_csv_path = Rails.root.join('db', 'seed_data', 'artist_info.csv')
  e_greeting_cards_csv_path = Rails.root.join('db', 'seed_data', 'egc_all.csv')
  artists = []
  CSV.foreach(artists_csv_path, headers: true, encoding: 'iso-8859-1:utf-8') do |row|
    params = {
      first_name: row['Artist_Name'].split(' ').first,
      last_name: row['Artist_Name'].split(' ').last,
      avatar_url: row['Artist_IMG_URL'],
      genre: row['Artist_Genre'],
      bio: row['Artist_Bio'],
      website: row['Artist_Website'],
    }

    artists << Artist.create!(params)
  end

  CSV.foreach(e_greeting_cards_csv_path, headers: true, encoding: 'iso-8859-1:utf-8') do |row|
    artist = artists.select { |artist_| artist_.first_name == row['Artist'].split(' ').first }.first
    theme = GreetingCardTheme.find_or_create_by(name: row['Theme'])
    params = {
      name: row['Product Title'],
      image_url: row['IMG_URL'],
      thumb_url: row['THUMBNAIL_URL'],
      theme: theme,
    }
    greeting_card = artist.greeting_cards.create!(params)
    occasions = row['Occasions']&.split(', ')
    categories = []
    occasions&.each do |occasion|
      categories << GreetingCardCategory.find_or_create_by(name: occasion)
    end
    greeting_card.categories = categories
  end
end
