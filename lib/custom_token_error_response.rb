module CustomTokenErrorResponse
  def body
    {
      errors: [
        {
          key: name,
          message: description,
        }.reject { |_, v| v.blank? },
      ],
      status: Rack::Utils.status_code(:unauthorized),
    }
  end
end
