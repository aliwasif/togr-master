class Session < ApplicationRecord
  has_many :items, dependent: :destroy
  accepts_nested_attributes_for :items, :allow_destroy => true
  has_many :session_status, as: :statusable, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :users_sessions, class_name: 'UserSession'
  has_many :users, through: :users_sessions, dependent: :destroy
  has_many :orders, dependent: :destroy

  belongs_to :session_request, optional: true
  belongs_to :worker, class_name: 'User', foreign_key: :worker_id, optional: true
  belongs_to :user, class_name: 'User', foreign_key: :user_id, optional: true

  scope :not_canceled, -> { where.not(status: Status::CANCEL) }

  before_save :set_expiry
  before_create :set_expiry

  delegate :paid, to: :items, prefix: true

  include PublicActivity::Model
  tracked
  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :destroy

  SESSION_EXPIRY_IN_DAYS = 14

  module Status
    ALL = [
      INITIATED = 0,
      PENDING = 1,
      ACCEPTED = 2,
      ARRIVED = 3,
      DECLINED = 4,
      CANCEL = 5,
      COMPLETED = 6,
      BLOCKED = 7,
    ].freeze

    DESCRIPTIVE = {
        INITIATED: 0,
        PENDING: 1,
        ACCEPTED: 2,
        ARRIVED: 3,
        DECLINED: 4,
        CANCEL: 5,
        COMPLETED: 6,
        BLOCKED: 7,
    }.freeze
  end

  module Types
    ALL = [
      INSTANT = 'instant',
      REGULAR = 'regular',
    ].freeze
  end

  def send_start_session_notification
    PushNotifications::SessionRequestPushNotifications.start_a_session(self)
  end

  def send_completed_session_notification
    PushNotifications::SessionRequestPushNotifications.send_completed_session_notification(self)
  end

  def send_cancel_session_push_notification
    PushNotifications::SessionRequestPushNotifications.cancel_a_session(self)
  end

  def send_purchased_push_notification
    PushNotifications::SessionRequestPushNotifications.purchased_a_session(self)
  end

  def signed_urls(items)
    AwsServices::StorageService.new(items).call
  end

  def send_photos_uploaded_notification
    PushNotifications::SessionPushNotifications.photos_uploaded(self)
  end

  def set_expiry
    self.expiry_date = SESSION_EXPIRY_IN_DAYS.days.from_now if expiry_date.blank?
  end

  def canceled?
    status == Status::CANCEL
  end

  def as_json(options = {})
    super(
      include: [:session_status, :items, :reviews]
    ).merge(
      {
        user: user.card,
        worker: worker.card,
        package: session_request&.package,
        paid_items_count: paid_items_count,
        orders: orders.pluck(:id),
        orders_users_ids: orders.pluck(:user_id)
      }
    )
  end

  def paid_items_count
    items.paid.count
  end

  def create_user_sessions(users_ids)
    users = User.find(users_ids)
    session_user = user
    users.each do |user|
      unless user.worker? && user.id != session_user.id
        user_session = UserSession.create!(
          user: user,
          worker: worker,
          session: self
        )
        user_session.send_add_user_to_session_notification
      end
    end
  end

  def self.find_by_user_or_worker(id, user_id)
    user_session = UserSession.where("session_id = ? AND (user_id = ? OR worker_id = ?)", id, user_id, user_id).first
    session = user_session.session
    if session.worker.id == user_id || user_session.user.id == user_id
      session
    else
      raise ArgumentError, I18n.t("session.errors.invalid")
    end
  end

  def mark_recommended(items, recommended_items)
    items.each do |item|
      recommended = recommended_items.include?(item)
      self.items.create!(s3_id: item, recommended: recommended)
    end
  end

  def card
    {
      id: id,
      user_id: user_id,
      worker_id: worker_id,
      session_request_id: session_request_id,
      created_at: created_at,
      completed_at: completed_at,
      expiry_date: expiry_date,
      worker: {
        id: worker.id,
        rating: worker.average_rating,
        phone_numebrs: worker.phone_numbers,
        location: worker.location,
        profile_pic: user.profile_pic,
      },
      user: {
        full_name: user.full_name,
        profile_pic: user.profile_pic,
        phone_number: user.phone_numbers,
        location: user.location,
      }
    }
  end

  def worker_rating
    worker.average_rating
  end
end
