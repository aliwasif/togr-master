class BusinessProfile < ApplicationRecord
  has_many :assets, as: :imageable, dependent: :destroy
  has_one :location, as: :locatable, dependent: :destroy
  belongs_to :service
  belongs_to :user

  scope :starts_with, -> (title) { where("title like ?", "%#{title}%")}

  module BUSINESS_TYPES
    ALL = [
      FREELANCER = 0,
      CORPORATE = 1,
    ].freeze
  end

  module STATUSES
    ALL = [
      APPROVED = 'approved',
      DISAPPROVED = 'disapproved'
    ].freeze
  end
end
