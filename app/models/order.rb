class Order < ApplicationRecord
  belongs_to :user
  belongs_to :payment_method, optional: true
  belongs_to :session
  belongs_to :worker, class_name: 'User', foreign_key: :worker_id, optional: true
  belongs_to :package, optional: true

  has_many :order_items, dependent: :destroy
  has_one :payment, as: :chargeable, dependent: :destroy
  has_one :review

  include PublicActivity::Model
  tracked
  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :destroy
  delegate :worker_rating, to: :session, allow_nil: true

  before_create :set_package

  NOTIFICATIONS = {
    scheduled: 'scheduled',
    sent: 'sent',
    received: 'received',
    thank_you_note_posted: 'Thank you note posted',
    thank_you_note_received: 'Thank you note received',
  }.freeze

  module Status
    ALL = [
      PENDING = 0,
      PAYMENT_FAILED = 1,
      COMPLETED = 2,
      FAILED = 3,
    ].freeze

    DESCRIPTIVE = {
      PENDING: 0,
      PAYMENT_FAILED: 1,
      COMPLETED: 2,
      FAILED: 3,
    }.freeze
  end

  def set_package
    if package_id.nil? && session&.session_request&.package.present?
      self.package_id = session&.session_request&.package&.id
    elsif package_id.blank?
      raise ArgumentError, I18n.t("orders.errors.package_id")
    end
  end

  def calculate
    {
      session_start_time: session.created_at,
      session_end_time: session.completed_at,
      session_type: session.session_type,
      user_location: session.user.location,
      package: package,
      tip: tip,
      total_earning: total,
      reviews: session.reviews,
      rating: session.worker.average_rating,
      order: self.as_json(include: { order_items: { include: :item } } )
    }
  end

  def self.orders_by_role(user)
    if user.has_role?(Role::ROLES::WORKER)
      where(worker: user)
    elsif user.has_role?(Role::ROLES::USER)
      where(user: user)
    end
  end

  def session_package
    package || session&.session_request&.package
  end

  def worker_reviews
    session&.worker&.reviews
  end

  def charge
    Stripe::ChargeService.new(self).create_destination_charge
  end

  def capture
    destination_charge = Stripe::ChargeService.new(self).create_destination_charge
    if destination_charge&.id
      ch = Stripe::Charge.retrieve( destination_charge.id)
      ch.capture
      update!(captured: true)
      session.update!(expired: true) if user_id == session.user_id
      session.update!(purchased_at: DateTime.current.utc)
      send_receipt_notification
      session.send_purchased_push_notification
    else
      update!(status: Status::FAILED)
    end
  end

  def send_receipt_notification
    PushNotifications::OrderPushNotifications.receipt_notification(self)
    data = {
        user_name: user.full_name,
        session_start_time: session.created_at&.utc&.strftime("%B %e, %Y at %I:%M %p"),
        session_end_time: session.completed_at&.utc&.strftime("%B %e, %Y at %I:%M %p"),
        session_type: session.session_type,
        session_package_price: "$#{package.amount}",
        session_extra_photos_count: extra_photos_count,
        session_photos_price: "$#{extra_photos_count * package.extra_photos_price}",
        session_tip: "$#{tip}",
        session_total: "$#{total}",
    }
    SendgridMailer.send(user.email, data,'d-89131bee74684b9fb2ba2a04c3d64da8')
  end

  def amount_in_cents(amount)
    (amount && amount > 0) ? (amount * 100).to_i : 0
  end

  def total_worker_earnings
    if direct_download
      extra_photos_commission + tip + package.worker_remuneration
    else
      extra_photos_commission + tip
    end
  end

  def extra_photos_commission
    package.extra_photos_commission * extra_photos_count
  end

  def mark_as_failed
    update!(status: Status::FAILED)
  end

  private

  def set_order_number
    self.order_number = generate_order_number
    save
  end

  def generate_order_number
    prefix = "WP"
    timstamp = Time.now.to_i.to_s
    sub_order_id = id.to_s

    if ((timstamp.size - sub_order_id.size) - 3) > 0
      number = timstamp[0..((timstamp.size - sub_order_id.size) - 3)] + sub_order_id.to_s
    else
      number = sub_order_id.to_s
    end
    prefix + number
  end
end
