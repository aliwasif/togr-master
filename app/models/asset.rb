class Asset < ApplicationRecord
  mount_base64_uploader :image, AssetUploader

  belongs_to :imageable, polymorphic: true, optional: true

  validates :name, uniqueness: { scope: :imageable_id }, allow_nil: true
end
