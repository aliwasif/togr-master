class Address < ApplicationRecord
  belongs_to :addressable, polymorphic: true, optional: true
  scope :primary_address, -> { find_by(primary: true) }
  scope :validate_address, ->(id) { find(id) }
end
