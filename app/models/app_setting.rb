class AppSetting < ApplicationRecord
  has_many :assets, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :assets, :allow_destroy => true

  def as_json(options = {})
    super(
      include: { assets: { only: [:name], methods: [:asset_url, :asset_link] }},
      only: [
        :id,
        :name,
        :value,
        :description,
        :setting_type,
      ]
    )
  end
end