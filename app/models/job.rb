class Job < ApplicationRecord
  belongs_to :worker, class_name: 'User', foreign_key: :worker_id, optional: true
  belongs_to :user, class_name: 'User', foreign_key: :user_id, optional: true

  module STATUSES
    ALL = [
      ACCEPTED = 'accepted',
      REJECTED = 'rejected',
      STARTED = 'started',
      CONFIRM_STARTED = 'confirm_started',
      COMPLETED = 'completed',
      CONFIRM_COMPLETED = 'confirm_completed',
      CREATED = 'created',
      CANCEL = 'cancel',
    ].freeze
  end

  default_scope { order(created_at: :desc) }

  before_create :set_job_number
  before_save :set_job_number

  def set_job_number
    self.job_number = generate_job_number unless job_number
  end

  class << self
    def jobs_by_role(user)
      if user.has_role?(::Role::ROLES::USER.to_sym)
        where(user_id: user.id)
      elsif user.has_role?(::Role::ROLES::WORKER.to_sym)
        where(worker_id: user.id)
      else
        none
      end
    end
  end

  def generate_job_number
    prefix = "JN"
    timstamp = Time.now.to_i.to_s
    job_id = id.to_s

    if ((timstamp.size - job_id.size) - 3) > 0
      number = timstamp[0..((timstamp.size - job_id.size) - 3)] + job_id.to_s
    else
      number = job_id.to_s
    end
    prefix + number
  end
end
