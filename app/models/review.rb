class Review < ApplicationRecord
  belongs_to :worker, class_name: 'User', foreign_key: :worker_id, optional: true
  belongs_to :user, class_name: 'User', foreign_key: :user_id, optional: true
  belongs_to :session, optional: true
  belongs_to :order, optional: true

  include PublicActivity::Model
  tracked
  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :destroy
  validate :only_user_can_rate_worker
  validates :user_id, uniqueness: { scope: :session_id }, allow_nil: true

  def only_user_can_rate_worker
    worker = User.find(self.worker_id)
    user = User.find(self.user_id)
    unless user.has_role?(Role::ROLES::USER.to_sym) && worker.has_role?(Role::ROLES::WORKER.to_sym)
      errors.add(:user, "can only rate the worker.")
    end
  end

  def send_review_notification
    PushNotifications::ReviewPushNotifications.review_notification(self)
  end
end
