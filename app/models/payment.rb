class Payment < ApplicationRecord
  belongs_to :chargeable, polymorphic: true, optional: true
  belongs_to :user
end
