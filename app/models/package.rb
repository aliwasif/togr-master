class Package < ApplicationRecord
  has_many :assets, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :assets, :allow_destroy => true
  module TYPES
    ALL = [
      INSTANT = 'instant',
      REGULAR = 'regular',
    ].freeze
  end

  def card
    {
      id: id,
      package_type: package_type,
      expiry: expiry,
      amount: amount,
      session_length: session_length,
      worker_remuneration: worker_remuneration,
    }
  end
end
