class Role < ApplicationRecord
has_and_belongs_to_many :users, :join_table => :users_roles

module ROLES
  ALL = [
    ADMIN = 'admin',
    WORKER = 'worker',
    USER = 'user',
  ].freeze
end

belongs_to :resource,
           :polymorphic => true,
           :optional => true


validates :resource_type,
          :inclusion => { :in => Rolify.resource_types },
          :allow_nil => true

scopify
end
