class Item < ApplicationRecord
  belongs_to :session, optional: true
  scope :paid, -> { where(status: Item::STATUS::PAID) }

  module STATUS
    ALL = [
      PAID = 'paid',
      UNPAID = 'unpaid',
    ].freeze
  end

  def image_url
    if audio_note.attachment
      audio_note.attachment.service_url
    end
  end
end
