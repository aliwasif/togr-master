class Screenshot < ApplicationRecord
  belongs_to :user
  belongs_to :session
  belongs_to :item

  after_create :set_screenshot_flag

  include PublicActivity::Model
  tracked
  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :destroy


  def set_screenshot_flag
    session.update!(screenshot: true)
    item.update!(screenshot: true)
  end

  def send_screenshot_taken_notification
    PushNotifications::ScreenshotPushNotifications.send_screenshot_taken_push_notification(self)
  end
end
