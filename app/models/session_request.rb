class SessionRequest < ApplicationRecord
  belongs_to :user, class_name: 'User', foreign_key: :user_id, optional: true
  belongs_to :worker, class_name: 'User', foreign_key: :worker_id, optional: true
  belongs_to :package
  has_one :payment_method
  has_one :payment, as: :chargeable, dependent: :destroy

  before_validation :set_advance

  ADVANCE = 5

  include PublicActivity::Model
  tracked
  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :destroy

  module Status
    ALL = [
      INITIATED = 0,
      PENDING = 1,
      ACCEPTED = 2,
      ARRIVED = 3,
      DECLINED = 4,
      CANCEL = 5,
    ].freeze
  end

  def send_request_push_notification
    PushNotifications::SessionRequestPushNotifications.reqeust_a_session(self)
  end

  def send_cancel_push_notification
    PushNotifications::SessionRequestPushNotifications.cancel_a_session_request(self)
  end

  def send_accept_push_notification
    PushNotifications::SessionRequestPushNotifications.accept_a_session_request(self)
  end

  def charge
    Stripe::ChargeService.new(self).create_destination_charge
  end

  def charge_and_capture
    charge = Stripe::ChargeService.new(self).create_destination_charge
    charge.capture ? update!(captured: true) : mark_as_failed
  end

  def mark_as_failed
    update!(status: Status::FAILED)
  end

  def total_worker_earnings
    package.worker_remuneration
  end

  def card
    {
      id: id,
      user_id: user_id,
      worker_id: worker_id,
      package_id: package_id,
      user_location: user_location,
      worker_location: worker_location,
      worker_arrived_at_location: worker_arrived_at_location,
      status: status,
    }
  end

  def as_json(options = {})
    super(
    ).merge(
      {
        user: user.card,
        worker: worker&.card,
      }
    )
  end

  def expired?
    (Time.now.to_i  - created_at.to_i) > 120
  end

  private

  def set_advance
    self.advance = ADVANCE
  end
end
