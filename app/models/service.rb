class Service < ApplicationRecord
  include Filterable
  has_ancestry orphan_strategy: :destroy
  has_many :assets, as: :imageable, dependent: :destroy
  has_many :addresses, as: :addressable, dependent: :destroy
  accepts_nested_attributes_for :assets, :allow_destroy => true

  scope :main_services, -> { where(ancestry: nil) }
  scope :sub_services, ->(id) { where(ancestry: id) }

  def self.collection_services(id)
    if id
      Service.where("id != ?", id).map{|u| ["#{u.title_english}", u.id]}
    else
      Service.all.map{|u| ["#{u.title_english}", u.id]}
    end
  end

  before_validation :set_root_ancestors

  def set_root_ancestors
    self.ancestry = nil if ancestry.blank?
  end
end
