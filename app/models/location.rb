class Location < ApplicationRecord
  belongs_to :locatable, polymorphic: true, optional: true

  acts_as_mappable :default_units => :miles,
                   :default_formula => :sphere,
                   :distance_field_name => :distance,
                   :lat_column_name => :lat,
                   :lng_column_name => :lng

  RADIUS = 50

  before_validation :set_address

  class << self
    def services(params)
      origin = Geokit::LatLng.new(params[:lat], params[:lng])
      radius = params[:radius]
      title = params[:title]
      service_ids = JSON.parse(params[:service_ids]) if params[:service_ids]
      business_profiles = BusinessProfile.where(nil)
      radius = radius.present? ? radius : RADIUS
      business_profile_ids = Location.within(
        radius, origin: origin).where(
        locatable_type: BusinessProfile.name
      ).pluck(:locatable_id)
      business_profiles.where(
        id: business_profile_ids,
        service_id: service_ids,
        status: BusinessProfile::STATUSES::APPROVED
      ).starts_with(title)
    end
  end

  private

  def set_address
    return nil unless lat && lng
    results = Geocoder.search([lat, lng])
    self.address = (results.first.present? ? "#{results.first.city}, #{results.first.state}" : '')
  end
end
