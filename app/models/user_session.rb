class UserSession < ApplicationRecord
  self.table_name = "users_sessions"
  belongs_to :session
  belongs_to :worker, class_name: 'User', foreign_key: :worker_id, optional: true
  belongs_to :user, class_name: 'User', foreign_key: :user_id, optional: true
  validates :user, uniqueness: { scope: :session }

  include PublicActivity::Model
  tracked
  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :destroy

  def send_add_user_to_session_notification
    PushNotifications::UserSessionPushNotifications.added_user_to_session(self)
  end
end
