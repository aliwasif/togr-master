class PaymentMethod < ApplicationRecord

  belongs_to :user

  scope :primary_payment_method, -> { find_by(primary: true) }

  after_create :set_primary_payment_method

  def set_primary_payment_method
    if self.user.payment_methods.count == 1
      self.primary = true
      self.save
    elsif self.user.payment_methods.where(primary: true).count > 0
      self.primary = false
      self.save
    end
  end
end
