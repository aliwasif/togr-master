class ErrorLog < ApplicationRecord
  class << self
    def log(e, origin)
      create(error: e.to_json, origin_id: origin&.id, origin_type: origin.class.name)
    end
  end
end
