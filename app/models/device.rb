class Device < ApplicationRecord
  belongs_to :user
  validates :token, uniqueness: { scope: :user }
  validates :token, presence: true
end
