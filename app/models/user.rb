class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # has_secure_password
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable,
         :lockable, :timeoutable

  module OnboardingState
    ALL = [
      EMAIL_NOT_CONFIRMED = 0,
      EMAIL_CONFIRMED = 1,
      DRIVING_LICENSE_UPLOADED = 2,
      STRIPE = 3,
      USER_APPROVED = 4,
      USER_NOT_APPROVED = 5,
      ALL_COMPLETED = 6,
    ].freeze
  end

  module Statuses
    ALL = [
      DISAPPROVED = 0,
      APPROVED = 1,
    ].freeze
  end

  module DescriptiveStatuses
    ALL = [
      DISAPPROVED = 'DISAPPROVED',
      APPROVED = 'APPROVED',
    ].freeze
  end

  DRIVING_LICENSE = ['driving_license_front', 'driving_license_back']

  has_many :access_grants,
           class_name: "Doorkeeper::AccessGrant",
           foreign_key: :resource_owner_id,
           dependent: :destroy

  has_many :access_tokens,
           class_name: "Doorkeeper::AccessToken",
           foreign_key: :resource_owner_id,
           dependent: :destroy

  has_many :payment_methods, dependent: :destroy
  has_many :session_requests, dependent: :destroy
  has_many :jobs
  has_many :devices, dependent: :destroy
  has_many :user_sessions, class_name: 'UserSession', dependent: :destroy
  has_many :sessions, through: :user_sessions, dependent: :destroy
  has_many :addresses, as: :addressable, dependent: :destroy
  has_many :assets, as: :imageable, dependent: :destroy
  has_many :session_statuses, as: :statusable, dependent: :destroy
  has_one :location, as: :locatable, dependent: :destroy
  has_one :business_profile, dependent: :destroy
  has_many :reviews, dependent: :destroy, foreign_key: :worker_id
  has_many :orders, dependent: :destroy
  has_many :payments, dependent: :destroy
  has_many :screenshots, dependent: :destroy

  validates :uid, uniqueness: true, if: Proc.new { |a| a.uid.blank? }
  validates :email, uniqueness: true
  validates :worker_uid, uniqueness: true, on: :create
  # validates :phone_number, uniqueness: true, on: :update, allow_blank: true
  validates :first_name, :last_name, :phone_numbers, presence: true, on: :update
  # before_validation :format_phone_number

  delegate :primary_address, to: :addresses, allow_nil: true
  delegate :validate_address, to: :addresses, allow_nil: true
  delegate :validate_occasion, to: :occasions, allow_nil: true

  accepts_nested_attributes_for :assets, :allow_destroy => true
  accepts_nested_attributes_for :devices, :allow_destroy => true
  accepts_nested_attributes_for :location, :allow_destroy => true
  accepts_nested_attributes_for :addresses, :allow_destroy => true

  include PublicActivity::Model
  tracked
  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :destroy
  attr_accessor :user_role
  after_update :check_driving_license

  before_create :set_uid
  after_create :set_worker_uid
  after_create :assign_role

  def self.convert_base_64_to_attachment(data)
    AttachmentService.convert_base64_to_attachment(data)
  end

  def self.find_for_token_authentication(params = {})
    access = AccessGrant.find_access(params['access_token'])
    return access.user if access
  end

  def self.valid_reset_password_code?(original_token)
    reset_password_token = Devise.token_generator.digest(
      self, :reset_password_token, original_token
    )
    recoverable = find_or_initialize_with_error_by(:reset_password_token, reset_password_token)
    recoverable.persisted? && recoverable.reset_password_period_valid?
  rescue => e
    e.message
  end

  def send_status_notification
    PushNotifications::UserPushNotifications.status_notification(self)
  end

  def self.reset_password_by_token(attributes = {})
    original_token       = attributes[:reset_password_token]
    reset_password_token = Devise.token_generator.digest(
      self, :reset_password_token, original_token
    )

    recoverable = find_or_initialize_with_error_by(:reset_password_token, reset_password_token)

    if recoverable.persisted?
      if recoverable.reset_password_period_valid?
        recoverable.reset_password(attributes[:password], attributes[:password_confirmation])
      else
        recoverable.errors.add(:reset_password_token, :expired)
      end
    end

    recoverable.reset_password_token = original_token if recoverable.reset_password_token.present?
    recoverable
  end

  def self.from_omniauth(data, provider)
    where(provider: provider, uid: data['id']).or(where(email: data['email'])).
      first_or_create! do |user|
      user.email = data['email']
      user.password = SecureRandom.urlsafe_base64(6).tr('lIO0', 'sxyz')
      user.first_name = data['first_name']
      user.last_name = data['last_name']
      user.gender = get_gender(data['gender'])
      user.last_sign_in_at = Time.now.utc
      user.set_attachment_by_url(data['image'])

      # If you are using confirmable and the provider(app/api/v1/endpoints/user_endpoints/users.rb) you use validate emails,
      # uncomment the line below to skip the confirmation emails.
      user.skip_confirmation!
    end
  end

  def self.get_gender(gender)
    return 0 unless gender
    if gender == "male"
      1
    elsif gender == "female"
      2
    end
  end

  def self.create_anonymous_users(emails)
    emails.each do |email|
      generated_password = "12345678"
      user = User.new
      user.email = email
      user.user_role = Role::ROLES::USER
      user.password = generated_password
      user.onboarding_state = User::OnboardingState::EMAIL_NOT_CONFIRMED
      user.skip_confirmation_notification!
      if user.save!(validate: false)
        user.send_start_session_email
      end
    end
  end

  def check_driving_license
    licenses = assets.where(name: DRIVING_LICENSE).pluck(:name)
    if licenses.uniq == DRIVING_LICENSE
      self.onboarding_state = OnboardingState::DRIVING_LICENSE_UPLOADED
      self.save
    end
  end

  def assign_role
    # raise Exception.new('User role not defined.') unless ::Role.pluck(:name).include?(user_role)
    # add_role(user_role.to_sym) if self.roles.blank? && ::Role.pluck(:name).include?(user_role)
    add_role(user_role.to_sym) if self.roles.blank?
  end

  def profile_pic
    asset = assets.where(name: 'profile')&.first
    if asset.present?
      asset.image&.url
    end
  end

  def descriptive_status
    (status == Statuses::APPROVED ? DescriptiveStatuses::APPROVED : DescriptiveStatuses::DISAPPROVED)
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def send_reset_password_code
    token = set_reset_password_token
    send_reset_password_instructions_notification(token)
    token
  end

  def device_tokens
    devices&.pluck(:token)&.compact
  end

  def reset_password(new_password, new_password_confirmation)
    if new_password.present?
      self.password = new_password
      self.password_confirmation = new_password_confirmation
      save!(validate: false)
    else
      errors.add(:password, :blank)
      false
    end
  end

  def generate_confirmation_token
    if self.confirmation_token && !confirmation_period_expired?
      @raw_confirmation_token = self.confirmation_token
    else
      self.confirmation_token = @raw_confirmation_token = rand(10000..100000).to_s
      self.confirmation_sent_at = Time.now.utc
    end
  end

  def as_json(options = {})
    super(
      include: {
        addresses: {},
        assets: {},
        devices: {},
        location: {},
        roles: {},
        reviews: { include: { user: { only: [:first_name, :last_name], include: [:assets] } } }
      },
      only: [
        :first_name,
        :last_name,
        :dob,
        :gender,
        :phone_numbers,
        :email,
        :status,
        :id,
        :business_name,
        :business_type,
        :onboarding_state,
        :privacy_settings,
        :profile,
        :uid,
        :worker_uid,
        :available,
        :status,
        :stripe_customer_id,
      ],
      methods: [:average_rating]
    )
  end

  def average_rating
    self.reviews.average(:rating)&.round(2)
  end

  def business_profile_by_role
    if self.has_role?(Role::ROLES::WORKER)
      business_profile
    end
  end

  def set_driving_license(params)
    dl_frond_base_64_image = params[:user]&.dig('profile')&.dig('driving_license_front_base_64_image')
    dl_back_base_64_image = params[:user]&.dig('profile')&.dig('driving_license_back_base_64_image')
    set_driving_license_front(dl_frond_base_64_image)
    set_driving_license_back(dl_back_base_64_image)
  end

  def set_attachment_by_url(url)
    self.image = AttachmentService.attachment_from_url(url)
  end

  def set_onboarding_state
    if is_profile_valid? && is_address_added? && payment_method_added?
      self.onboarding_state = OnboardingState::ALL_COMPLETED
      save
    elsif is_profile_valid? && is_address_added? && !payment_method_added?
      self.onboarding_state = OnboardingState::PAYMETN_METHOD_EMPTY
      save
    elsif is_profile_valid? && is_address_added?
      self.onboarding_state = OnboardingState::ADDRESS_COMPLETE
      save
    elsif is_profile_valid? && !is_address_added?
      self.onboarding_state = OnboardingState::ADDRESS_EMPTY
      save
    end
  end

  def sessions_by_role
    if self.has_role? (Role::ROLES::WORKER)
      Session.where(worker: self)
    elsif self.has_role? (Role::ROLES::USER)
      sessions
    end
  end

  def sessions_by_filter(filter)
    if filter == 'today'
      sessions_by_role.where(created_at: DateTime.now.beginning_of_day...DateTime.now.end_of_day).order(created_at: :desc)
    elsif filter == 'purchased'
      if worker?
        paid_sessions_ids = Order.where(worker_id: id, captured: true).pluck(:session_id)
        sessions_by_role.where.not(purchased_at: nil).where(id: paid_sessions_ids).order(purchased_at: :desc)
      else
        paid_orders_ids  = payments.where(chargeable_type: "Order").pluck(:chargeable_id)
        paid_sessions_ids = Order.where(id: paid_orders_ids).order(:created_at).pluck(:session_id)
        sessions_by_role.where.not(purchased_at: nil).where(id: paid_sessions_ids).order(purchased_at: :desc)
      end
    else
      sessions_by_role.order(created_at: :desc)
    end
  end

  def session_by_role(session_id)
    if self.has_role? (Role::ROLES::WORKER)
      Session.not_canceled.where(id: session_id, worker: self)&.first
    elsif has_role?(Role::ROLES::USER)
      session = Session.not_canceled.find_by(id: session_id, user: self)
      if session.blank?
        user_session = UserSession.find_by(session_id: session_id, user_id: id)
        session = (user_session&.session&.canceled? ? nil : user_session&.session)
      end
      session
    end
  end

  def session_orders(session_id)
    orders.where(session_id: session_id) if session_id
  end

  def card
    {
      id: id,
      first_name: first_name,
      last_name: last_name,
      phone_numbers: phone_numbers,
      email: email,
      onboarding_state: onboarding_state,
      profile: profile,
      assets: assets.as_json(only: [:name, :s3_id]),
      rating: average_rating,
      location: location,
      worker_uid: worker_uid,
      status: status,
    }
  end

  def worker?
    has_role?(Role::ROLES::WORKER)
  end

  def set_uid
    self.uid =  generate_uid
  end

  def set_worker_uid
    self.worker_uid = generate_worker_uid
    self.save!(validate: false)
  end

  def generate_uid
    prefix = has_role?(Role::ROLES::WORKER) ? "TOGR" : "UR"
    timstamp = Time.now.to_i.to_s
    sub_order_id = id.to_s

    if ((timstamp.size - sub_order_id.size) - 3) > 0
      number = timstamp[0..((timstamp.size - sub_order_id.size) - 3)] + sub_order_id.to_s
    else
      number = sub_order_id.to_s
    end
    prefix + number
  end

  def generate_worker_uid
    "TOGR" +  ("%04d" % User.with_role(Role::ROLES::WORKER).count).to_s
  end

  def approved?
    status == User::Statuses::APPROVED
  end

  def disapproved?
    status == User::Statuses::DISAPPROVED
  end

  # Send confirmation instructions by email
  def send_confirmation_instructions
    unless @raw_confirmation_token
      generate_confirmation_token!
    end

    opts = pending_reconfirmation? ? { to: unconfirmed_email } : { to: email }
    SendgridMailer.send(opts[:to], { email: email, token: @raw_confirmation_token }, 'd-aaf0ec0ef63d468c835bb2a5f1b87a73')
  end

  def send_start_session_email
    SendgridMailer.send(email, { user_name: (full_name&.present? ? full_name : email) }, 'd-f731676fbdcb4175ad4245483962b32d')
  end

  protected

  def send_reset_password_instructions_notification(token)
    #send_devise_notification(:reset_password_instructions, token, {})
    SendgridMailer.send(email, {email: email, user_name: full_name, token: token }, 'd-07070ed1c4e64684bf3e065f82d67d4d')
  end

  def self.strip_phone_number(phone_number)
    phone_number&.gsub(/[[:space:]]|[^\d,\.]/, '')
  end

  private

  def send_notification_by_sms(message, to)
    return nil if to.blank?
    TwilioTextMessenger.new(message, to).call
  end

  def is_profile_valid?
    first_name.present? && last_name.present? && phone_numbers.present? && dob.present?
  end

  def is_address_added?
    addresses.any?
  end

  def payment_method_added?
    payment_methods.count > 0
  end

  def format_phone_number
    # self.phone_number = self.phone_number&.gsub(/[[:space:]]|[^\d,\.]/, '')
  end
end
