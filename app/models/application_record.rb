class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def attachment_url(attachment_name)
    return unless respond_to?(attachment_name)
    return unless public_send(attachment_name).attached?
    Rails.application.routes.url_helpers.rails_blob_url(public_send(attachment_name))
  end
end
