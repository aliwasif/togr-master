class CompleteSessionReminderWorker
  include Sidekiq::Worker

  def perform
    sessions = Session.joins(:items).distinct.where('sessions.expiry_date':  Date.tomorrow.beginning_of_day.utc..2.week.from_now.utc.end_of_day, 'items.status': nil)
    sessions.each do |session|
      PushNotifications::SessionPushNotifications.complete_session_reminder(session)
    end
  end
end
