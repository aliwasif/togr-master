require 'aws-sdk-s3'
module AwsServices
  class StorageService
    attr_accessor :items, :connection, :presigner

    BUCKET_NAME = "togr"

    def initialize(items)
      @items = items
      @connection = connection
      @presigner = Aws::S3::Presigner.new
    end

    def call
      signed_urls
    end

    private

    def signed_urls
      items.map{ |item| signed_url(item.s3_id) }
    end

    def connection
      Aws.config.update(
        region: 'us-west-2',
        credentials: Aws::Credentials.new(
          Rails.application.credentials[Rails.env.to_sym][:s3][:access_key_id],
          Rails.application.credentials[Rails.env.to_sym][:s3][:secret_access_key]
        )
      )
    end

    def signed_url(key)
      presigner.presigned_url(
        :get_object,
        bucket: BUCKET_NAME,
        key: key,
        expires_in: 7.days.to_i
      ).to_s
    end
  end
end
