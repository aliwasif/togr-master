module ExternalAuthorization
  class Facebook
    def initialize(auth_code)
      @auth_code = auth_code
      @user_data = user_data
      @image_url = image_url
    end

    def user_data
      fields = "fields=id,name,email,first_name,last_name,gender"
      url = "https://graph.facebook.com/v3.2/me?access_token=#{@auth_code}"
      access_token_url = URI.parse("#{url}&#{fields}")
      response = Net::HTTP.get_response(access_token_url)
      body = JSON.parse(response.body)
      body['image'] = image_url
      body
    end

    def user_data_req
      "#{access_token_url}=#{@auth_code}&fields=id,name,email,first_name,last_name,gender"
    end

    def image
      dimensions = "width=180&height=180&redirect=false"
      img = URI.parse("#{picture_url}?access_token=#{@auth_code}&#{dimensions}")
      response = Net::HTTP.get_response(img)
      JSON.parse(response.body)
    end

    def image_url
      image['data'].present? ? image['data']['url'] : nil
    end

    def access_token_url
      "https://graph.facebook.com/v2.5/me?access_token"
    end

    def picture_url
      "https://graph.facebook.com/v2.5/me/picture"
    end

    def get_user!
      if user_data.present? && user_data['id'].present?
        begin
          User.from_omniauth(@user_data, "facebook")
        rescue => e
          e.message
          nil
        end
      else
        nil
      end
    end
  end
end
