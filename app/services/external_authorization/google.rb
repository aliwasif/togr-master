module ExternalAuthorization
  class Google
    def initialize(access_token)
      @access_token = access_token
      @user_data = user_data
    end

    def verify_access_token
      uri = URI.parse("https://www.googleapis.com/oauth2/v3/tokeninfo")
      params = { access_token: @access_token }
      response = Net::HTTP.post_form(uri, params)
      data = JSON.parse(response.body)
      data['aud'].present? && data['email']
    end

    def user_data
      if verify_access_token
        url = "https://www.googleapis.com/plus/v1/people/me?access_token=#{@access_token}"
        google = URI.parse(url)
        response = Net::HTTP.get_response(google)
        parsed(JSON.parse(response.body))
      else
        nil
      end
    end

    def get_user!
      if @user_data.present?
        begin
          User.from_omniauth(@user_data, "google")
        rescue => e
          ErrorMailer.send_error_trace(e.message).deliver
          nil
        end
      else
        nil
      end
    end

    def parsed(response)
      {
        'id': response['id'],
        'email': response['emails']&.first['value'],
        'gender': response['gender'],
        'first_name': response['name']['givenName'],
        'last_name': response['name']['familyName'],
        'image': response['image']['url'],
      }
    end
  end
end
