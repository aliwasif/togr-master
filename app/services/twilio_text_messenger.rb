class TwilioTextMessenger
  attr_reader :message, :to

  def initialize(message, to)
    @message = message
    @to = "+#{to}"
  end

  def call
    return if Rails.env.test?
    client = Twilio::REST::Client.new
    client.messages.create(
      {
        from: Rails.application.credentials.send(Rails.env.to_sym)[:twilio_phone_number],
        to: to,
        body: message,
      }
    )
  rescue => e
    ErrorMailer.send_error_trace(e.message).deliver
  end
end
