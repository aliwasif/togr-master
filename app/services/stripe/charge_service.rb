module Stripe
  class ChargeService
    DEFAULT_CURRENCY = 'usd'.freeze

    def initialize(chargeable)
      @chargeable_entity = chargeable
      @user = chargeable_entity.user
      @worker = chargeable_entity.worker
      @package = chargeable_entity.package
      @amount = amount_to_charge
      @stripe_customer_id = user.stripe_customer_id
    end

    def call
      charge(find_customer)
    end

    def create_destination_charge
      destination_charge(find_customer)
    end

    def create_advance_charge
      advance_charge(find_customer)
    end

    private

    attr_accessor :user, :worker, :stripe_customer_id, :package, :amount, :chargeable_entity

    def find_customer
      if user.stripe_customer_id
        retrieve_customer
      end
    end

    def amount_in_cents(amount)
      (amount && amount > 0) ? (amount * 100).to_i : 0
    end

    def retrieve_customer
      Stripe::Customer.retrieve(stripe_customer_id)
    end

    def retrieve_charge(charge_id)
      Stripe::Charge.retrieve(charge_id)
    end

    def charge(customer)
      if amount_in_cents(amount) > 0
        charge = Stripe::Charge.create(
          customer: customer.id,
          amount: amount_in_cents(amount),
          description: chargeable_entity.id.to_s,
          currency: DEFAULT_CURRENCY,
          metadata: {
            chargeable_type: chargeable_entity.class.name,
            chargeable_id: chargeable_entity.id,
            customer_email: customer.email
          },
          capture: false,
        )
        create_payment(charge) if charge["paid"]
      end
    rescue Stripe::CardError => e
      ErrorLog.log({ exception: e, trace: e.backtrace }, customer)
    end

    def advance_charge(customer)
      charge = Stripe::Charge.create(
        customer: customer.id,
        amount: amount_in_cents(chargeable_entity.advance),
        description: chargeable_entity.id.to_s,
        currency: DEFAULT_CURRENCY,
        metadata: {
          chargeable_type: chargeable_entity.class.name,
          chargeable_id: chargeable_entity.id,
          customer_email: customer.email
        },
        capture: false,
      )
      create_payment(charge) if charge["paid"]
    rescue Stripe::CardError => e
      ErrorLog.log({ exception: e, trace: e.backtrace }, customer)
    end

    def destination_charge(customer)
      charge = nil
      if amount > 0 && worker.stripe_customer_id
        charge = Stripe::Charge.create(
          customer: customer.id,
          amount: amount_in_cents(amount),
          description: chargeable_entity.id.to_s,
          currency: DEFAULT_CURRENCY,
          transfer_data: {
            amount: amount_in_cents(chargeable_entity.total_worker_earnings),
            destination: worker.stripe_customer_id,
          },
          metadata: {
            chargeable_type: chargeable_entity.class.name,
            chargeable_id: chargeable_entity.id,
            customer_email: customer.email
          },
          capture: false,
        )
        create_payment(charge) if charge["paid"]
      end
      charge
    rescue Stripe::CardError => e
      ErrorLog.log({ exception: e, trace: e.backtrace }, customer)
      nil
    end

    def create_payment(charge)
      payment = chargeable_entity.build_payment(
        charge_id: charge['id'],
        amount: amount,
        user: user
      )
      payment.save!
      payment.reload
    end

    def amount_to_charge
      case chargeable_entity.class.name
        when ::Order.name
          if chargeable_entity.direct_download
            chargeable_entity.total
          else
            chargeable_entity.total - chargeable_entity.package.amount
          end
        when ::SessionRequest.name
          chargeable_entity.package.amount
        else
          0
      end
    end
  end
end
