module Stripe
  class UtilityService

    class << self
      def customer(user)
        return Stripe::Customer.retrieve(user.stripe_customer_id) if user.stripe_customer_id
        customer = Stripe::Customer.create(email: user.email, metadata:
          { name: user.full_name, phone_number: user.phone_number })
        user.update!(stripe_customer_id: customer.id)
        customer
      end

      def transactions(user)
        charges = Stripe::Charge.all(customer: user.stripe_customer_id)
        dates = {}
        order_total = 0
        order_service_charges = 0
        charges.each do |charge|
          charge_date = :"#{Time.at(charge['created']).strftime("%Y-%m-%d")}"
          transaction = {}
          dates[charge_date] = [] if dates[charge_date].blank?
          order = ::Order.where(id: charge["metadata"]["order"]).first

          transaction['amount'] = charge['amount']
          transaction['created'] = charge_date
          transaction['order_summary'] = {
            id: order.id,
            date: charge_date,
            payment_info: {
              last4: order.payment_method.json['last4'],
              brand: order.payment_method.json['brand']
            },
            sent_to: order.recipient,
            shipping_address: Address.find(order.address_id),
            status: ::Order::Status::DESCRIPTIVE.key(order.status.to_i)
          }

          transaction['order_items'] = []
          order.sub_orders.each do |sub_order|
            transaction['order_items'] = transaction['order_items'] + sub_order.order_items.as_json(include: [:vendor_item])
          end

          transaction['order_total_summary'] = {}
          transaction['order_total_summary']['total'] = calculate_order_total(order)
          transaction['order_total_summary']['service_charges'] = calculate_service_charges(order)

          dates[charge_date] << transaction
        end
        dates
      end

      def calculate_order_total(order)
        service = Stripe::ChargeService.new(order)
        service.calculate_total_charge
      end

      def calculate_service_charges(order)
        0
      end
    end
  end
end
