module Stripe
  class TransferService
    DEFAULT_CURRENCY = 'usd'.freeze

    def initialize(order)
      @order = order
      @package = order.package
      @amount = amount_in_cents
      @stripe_worker_id = order.worker.profile['stripe_account_id']
    end

    def call
      transfer
    end

    private

    attr_accessor :stripe_worker_id, :package, :amount, :order

    def amount_in_cents
      (total_worker_earnings * 100).to_i
    end

    def transfer
      if amount > 0
        transferred = Stripe::Transfer.create(
          {
            amount: amount,
            currency: "usd",
            destination: stripe_worker_id,
            metadata: {
              tip: tip,
              worker_remuneration: worker_remuneration,
              extra_photos_commission: extra_photos_commission
            },
            transfer_group: order.id,
          }
        )
      end
    rescue => e
      ErrorLog.log({ exception: e, trace: e.backtrace }, order)
    end

    def total_worker_earnings
      worker_remuneration + extra_photos_commission + tip
    end

    def tip
      order.tip
    end

    def worker_remuneration
      package.worker_remuneration
    end

    def extra_photos_commission
      package.extra_photos_commission * order.extra_photos_count
    end

    def create_transfer(charge)
    end
  end
end
