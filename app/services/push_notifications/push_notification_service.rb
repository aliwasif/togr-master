module PushNotifications
  class PushNotificationService

    attr_accessor :device_tokens, :data, :user_ids, :workers_tokens, :users_tokens

    def initialize(device_tokens, data)
      @device_tokens = device_tokens
      @data = data
      @user_ids = find_users_id
      @users_tokens = find_users_by_tokens
      @workers_tokens = find_workers_by_tokens
    end

    def call
      notify
    end

    private

    def notify
      if device_tokens.count > 0
        users_push_notification_service if @users_tokens.any?
        workers_push_notification_service if @workers_tokens.any?
      end
    end

    def users_push_notification_service
      push = PushNotifications::Notify.new(user_credentials.app_id, user_credentials.auth_token)
      push.send(users_tokens, data)
    end

    def workers_push_notification_service
      push = PushNotifications::Notify.new(worker_credentials.app_id, worker_credentials.auth_token)
      push.send(@workers_tokens, data)
    end

    def find_users_id
      Device.where(token: device_tokens).pluck(:user_id)
    end

    def find_users_by_tokens
      tokens = []
      users_ids = User.with_role(:user).where(id: user_ids).pluck(:id)
      tokens = Device.where(user_id: users_ids).pluck(:token) if users_ids.any?
      tokens
    end

    def find_workers_by_tokens
      tokens = []
      workers_ids = User.with_role(:worker).where(id: user_ids).pluck(:id)
      tokens = Device.where(user_id: workers_ids).pluck(:token) if workers_ids.any?
      tokens
    end

    def user_credentials
      OpenStruct.new(
        app_id: Rails.application.credentials[Rails.env.to_sym][:onesignal][:user][:app_id],
        auth_token: Rails.application.credentials[Rails.env.to_sym][:onesignal][:user][:auth_token]
      )
    end

    def worker_credentials
      OpenStruct.new(
        app_id: Rails.application.credentials[Rails.env.to_sym][:onesignal][:worker][:app_id],
        auth_token: Rails.application.credentials[Rails.env.to_sym][:onesignal][:worker][:auth_token]
      )
    end
  end
end
