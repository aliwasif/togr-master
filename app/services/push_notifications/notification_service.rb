module PushNotifications
  class NotificationService
    class << self
      def create_activity(source, owner, recipient, key, data=nil)
        source.create_activity key: key, owner: owner, recipient: recipient, parameters: data
      end

      def nearest_worker(session_request)
        if session_request.worker && !session_request.declined_users.include?(session_request.worker.id)
          session_request.worker
        else
          firebase = FirebaseServices::GeoService.new(session_request.user_location)
          workers = firebase.nearby(declined_users: session_request.declined_users)
          worker = workers&.compact&.sort_by { |v| v[:distance] }&.first
          worker.present? ? User.find( worker["worker_id"]) : nil
        end
      end
    end
  end
end