module PushNotifications
  class SessionRequestPushNotifications < NotificationService

    def self.accept_a_session_request(session_request)
      push_key = 'worker_accepted'
      alert = 'Accept a session.'
      user = session_request.user
      worker = session_request.worker
      data = {
        alert: alert,
        key: push_key,
        session_request: session_request.card,
        worker: worker.card,
        package: session_request.package.card
      }
      PushNotificationService.new(user.device_tokens, data).call
      create_activity(session_request, worker, user, push_key, data)
    end

    def self.reqeust_a_session(session_request)
      push_key = 'session_request'
      alert = 'Request a Session.'
      user = session_request.user
      worker = nearest_worker(session_request)
      if worker&.device_tokens
        data = {
          alert: alert,
          key: push_key,
          session_request: session_request.card,
          user: user.card,
          package: session_request.package.card
        }
        session_request.update!(worker: worker)
        PushNotificationService.new(worker.device_tokens, data).call
        create_activity(session_request, user, worker, push_key, data)
      else
        if user&.device_tokens
          data = {
            alert: I18n.t("session_request.errors.nearest_worker"),
            key: push_key,
            session_request: session_request.card,
            user: user.card,
            package: session_request.package.card
          }
          PushNotificationService.new(user.device_tokens, data).call
          create_activity(session_request, user, user, push_key, data)
        end
      end
    end

    def self.cancel_a_session_request(session_request)
      push_key = 'request_cancelled'
      alert = I18n.t("session_request.cancel_session")
      user = session_request.user
      worker = session_request.worker
      data = {
        alert: alert,
        key: push_key,
        session_request: session_request.card,
        user: user.card,
        worker: worker.card
      }
      PushNotificationService.new(user.device_tokens, data).call
      PushNotificationService.new(worker.device_tokens, data).call
      create_activity(session_request, user, worker, push_key, data)
      create_activity(session_request, user, user, push_key, data)
    end

    def self.start_a_session(session)
      push_key = 'session_started'
      alert = "Your session has started you’ll get notified when it's finished (start session)"
      session_request = session.session_request
      package = session_request&.package
      data = {
        alert: alert,
        key: push_key,
        session: session.card,
        package: package&.card
      }
      PushNotificationsJob.perform(session.user.device_tokens, data)
      PushNotificationsJob.perform(session.worker.device_tokens, data)
      create_activity(session, session_request&.user, session_request&.worker, push_key, data)
    end

    def self.cancel_a_session(session)
      push_key = 'session_cancel'
      alert = "Unfortunalty, your session has cancelled."
      session_request = session.session_request
      package = session_request&.package
      data = {
        alert: alert,
        key: push_key,
        session: session.card,
        package: package&.card
      }
      PushNotificationsJob.perform(session.user.device_tokens, data)
      PushNotificationsJob.perform(session.worker.device_tokens, data)
      create_activity(session, session_request&.user, session_request&.worker, push_key, data)
    end

    def self.purchased_a_session(session)
      push_key = 'new_receipt_available'
      alert = I18n.t("session.purchased_a_session", { photos_count: session.paid_items_count})
      user = session.user
      worker = session.worker
      data = {
        alert: alert,
        key: push_key,
        session: session.card,
        user: user.card
      }
      PushNotificationsJob.perform(worker.device_tokens, data)
      create_activity(session, user, worker, push_key, data)
    end

    def self.send_completed_session_notification(session)
      push_key = 'session_completed'
      alert = "Your session has been completed."
      data = {
        alert: alert,
        key: push_key,
        session: session.card
      }
      PushNotificationsJob.perform(session.worker.device_tokens, data)
      PushNotificationsJob.perform(session.user.device_tokens, data)
      create_activity(session, session.user, session.worker, push_key, data)
    end
  end
end
