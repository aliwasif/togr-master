module PushNotifications
  class UserPushNotifications < NotificationService

    def self.status_notification(user)
      push_key = user.descriptive_status
      alert = "Your account has been #{user.descriptive_status}."
      data = {
        alert: alert,
        key: push_key,
        user: user.card,
      }
      PushNotificationService.new(user.device_tokens, data).call
      create_activity(user, user, user, push_key, data)
    end
  end
end
