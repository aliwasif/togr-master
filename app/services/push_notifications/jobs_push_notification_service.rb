module PushNotifications
  class JobsPushNotificationService < NotificationService

    attr_accessor :user, :job

    def initialize(user, job)
      @user = user
      @job = job
    end

    def send_new_job
      push_key = 'job_created'
      alert = "You have a new job request."
      data = {
        alert: alert,
        key: push_key,
        job: job,
        business_profile: job.worker.business_profile
      }
      PushNotificationsJob.perform(job.user.devices.pluck(:token), data)
    end

    def send_start_job
      push_key = 'job_started'
      alert = "#{job.worker.full_name} has arrived and requested to start job."
      data = {
        alert: alert,
        key: push_key,
        job: job,
        business_profile: job.worker.business_profile
      }
      PushNotificationsJob.perform(job.user.devices.pluck(:token), data)
    end

    def send_end_job
      push_key = 'job_completed'
      alert = "#{job.worker.full_name} ended the job, requested to end the job."
      data = {
        alert: alert,
        key: push_key,
        job: job,
        business_profile: job.worker.business_profile
      }
      PushNotificationsJob.perform(job.user.devices.pluck(:token), data)
    end

    def send_confirm_completed_job
      push_key = 'job_confirm_completed'
      alert = "#{job.user.full_name} approved your end job request, Job Completed."
      data = {
        alert: alert,
        key: push_key,
        job: job,
        business_profile: job.worker.business_profile
      }
      PushNotificationsJob.perform(job.worker.devices.pluck(:token), data)
    end

    def send_confirm_start_job
      push_key = 'job_confirm_started'
      alert = "#{job.user.full_name} approved your start job request, Job Started."
      data = {
        alert: alert,
        key: push_key,
        job: job,
        business_profile: job.worker.business_profile
      }
      PushNotificationsJob.perform(job.worker.devices.pluck(:token), data)
    end

    def send_accept_job
      push_key = 'job_accepted'
      alert = "#{job.user.full_name} has accepted your offer."
      data = {
        alert: alert,
        key: push_key,
        job: job,
        business_profile: job.worker.business_profile
      }
      PushNotificationsJob.perform(job.worker.devices.pluck(:token), data)
    end

    def send_reject_job
      push_key = 'job_rejected'
      alert = "#{job.user.full_name} decline your offer."
      data = {
        alert: alert,
        key: push_key,
        job: job,
        business_profile: job.worker.business_profile
      }
      PushNotificationsJob.perform(job.worker.devices.pluck(:token), data)
    end

    def send_cancel_job_by_user
      push_key = 'job_canceled'
      alert = "#{job.user.full_name} has cancelled your job."
      data = {
        alert: alert,
        key: push_key,
        job: job,
        business_profile: job.worker.business_profile
      }
      PushNotificationsJob.perform(job.worker.devices.pluck(:token), data)
    end

    def send_cancel_job_by_worker
      push_key = 'job_canceled'
      alert = "#{job.worker.full_name} withdrawn from job."
      data = {
        alert: alert,
        key: push_key,
        job: job,
        business_profile: job.worker.business_profile
      }
      PushNotificationsJob.perform(job.user.devices.pluck(:token), data)
    end
  end
end
