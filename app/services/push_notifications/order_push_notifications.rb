module PushNotifications
  class OrderPushNotifications < NotificationService
    def self.receipt_notification(order)
      user = order.user
      worker = order.worker
      push_key = 'new_receipt_available'
      alert = "New Session receipt available for session #{order.session.id}."
      data = {
        alert: alert,
        key: push_key,
        order: order,
      }
      PushNotificationService.new(user.device_tokens, data).call
      PushNotificationService.new(worker.device_tokens, data).call
      create_activity(order, user, worker, push_key, data)
    end
  end
end
