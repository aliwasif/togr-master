module PushNotifications
  class SessionPushNotifications < NotificationService
    class << self
      def photos_uploaded(session)
        push_key = 'photos_uploaded'
        alert = "Your togr finished uploading your photos."
        data = {
          alert: alert,
          key: push_key,
          session: session.card
        }
        PushNotificationsJob.perform(session.user.device_tokens, data)
        create_activity(session, session.user, session.worker, push_key, data)
      end

      def complete_session_reminder(session)
        push_key = 'session_will_expire_soon'
        alert = I18n.t("session.complete_session_reminder", { days: (session.expiry_date.to_date - Date.today).to_i } )
        user = session.user
        data = {
          alert: alert,
          key: push_key,
          session: session.card,
          user: user.card,
        }
        PushNotifications::PushNotificationService.new(user.device_tokens, data).call
        create_activity(session, user, user, push_key, data)
      end
    end
  end
end
