module PushNotifications
  class ReviewPushNotifications < NotificationService

    def self.review_notification(review)
      user = review.user
      worker = review.worker
      push_key = 'review'
      alert = "#{user.full_name} rated you #{review.rating} stars."
      data = {
        alert: alert,
        key: push_key,
        user: user.card,
      }
      PushNotificationService.new(worker.device_tokens, data).call
      create_activity(review, user, worker, push_key, data)
    end
  end
end
