module PushNotifications
  class UserSessionPushNotifications < NotificationService
    class << self
      def added_user_to_session(user_session)
        push_key = 'added_to_session'
        alert = "You’ve been added to a session, view it in your session history!."
        data = {
          alert: alert,
          key: push_key,
          session: user_session.session.card
        }
        PushNotificationsJob.perform(user_session.user.device_tokens, data)
        create_activity(user_session, user_session.user, user_session.worker, push_key, data)
      end
    end
  end
end
