module PushNotifications
  class ScreenshotPushNotifications < NotificationService

    def self.send_screenshot_taken_push_notification(screenshot)
      push_key = 'screenshot_taken'
      worker = screenshot.session.worker
      user = screenshot.user
      session = screenshot.session
      alert = "#{user.full_name} took a screenshot of session (#{session.id}) photo."
      data = {
        alert: alert,
        key: push_key,
        screenshot: screenshot,
        user: user.card,
      }
      PushNotificationService.new(worker.device_tokens, data).call
      create_activity(screenshot, worker, user, push_key, data)
    end
  end
end
