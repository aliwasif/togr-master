module PushNotifications
  class Notify
    include HTTParty
    attr_reader :app_id, :auth_token, :body


    def initialize(app_id, auth_token)
      @app_id = app_id
      @auth_token = auth_token
      @body = body
    end

    HEADERS = { 'Authorization': "Basic #{@auth_token}", 'Content-Type': 'application/json' }
    ONE_SIGNAL_URI = URI.parse('https://onesignal.com/api/v1/notifications')

    # Create new file to storage log of pushes.
    @push_logger = ::Logger.new(Rails.root.join('log', 'push.log'))

    # Every request needs to inform the APP ID.
    def body
      { app_id: @app_id }
    end

    def send_push(body)
      HTTParty.post ONE_SIGNAL_URI, headers: HEADERS, body: body, logger: @push_logger, log_level: :debug, log_format: :curl
    end

    # Send push to all users.
    def self.send_all(data)
      push_body = @body.merge(
        {
          "included_segments" => ["All"],
          "data" => data,
          "content_available" => true,
          "contents" => { en: data[:alert] }
        }).to_json

      send_push(push_body)
    end

    def send(device_tokens, data)
      push_body = @body.merge(
        {
          "include_player_ids" => device_tokens,
          "data" => data,
          "content_available" => true,
          "contents" => { en: data[:alert] }
        }).to_json

      send_push(push_body)
    end
  end
end
