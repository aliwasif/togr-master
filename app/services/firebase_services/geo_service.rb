module FirebaseServices
  class GeoService
    RADIUS = 1

    def initialize(location, options={})
      @location = Geokit::LatLng.new(location['lat'], location['lon'])
      @radius = options.dig(:radius) || RADIUS
      @units = options.dig(:units) || :kms
    end

    def nearby(options={})
      bounds = Geokit::Bounds.from_point_and_radius(location, radius, units: units)
      declined_users = options.dig(:declined_users) ? options.dig(:declined_users) : []
      locations.map do |key, loc|
        point = geo_point(loc['lat'], loc['lang'])
        if online_status(loc['online_status']) && active_session(loc['active_session']) && !declined_users.include?(key.to_i) && bounds.contains?(point)
          locations[key]
        end
      end&.compact
    end

    private

    attr_accessor :location, :radius, :units

    def geo_locations
      locations.map{|key, val| Geokit::LatLng.new(val['lat'], val['lang'])}&.compact
    end

    def online_status(status)
      status == "true"
    end

    def active_session(status)
      status == "false"
    end

    def geo_point(lat, lng)
      Geokit::LatLng.new(lat, lng)
    end

    def locations
      response = connection.get('location', { 'location/active_session': false })
      response.body
    end

    def connection
      Firebase::Client.new(base_uri, private_key_json_string)
    end

    def base_uri
      'https://togr-90efa.firebaseio.com/'
    end

    def private_key_json_string
      File.open(Rails.root.join('config', 'togr-user-firebase-adminsdk-oymew-4a0ff149c9.json')).read
    end
  end
end
