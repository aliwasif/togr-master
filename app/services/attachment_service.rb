require 'open-uri'
class AttachmentService
  class << self
    def split_base64(string)
      content = string.match(%r{^data:(.*?);(.*?),(.*)$})
      if content
        uri = {}
        uri[:type] = content[1]
        uri[:encoder] = content[2]
        uri[:data] = string.split('base64,').last
        uri[:extension] = content[1].split('/')[1]
        uri
      else
        nil
      end
    end

    def convert_base64_to_attachment(string)
      tmp_file = convert_base64_to_tmp_file(string)
      file_info = split_base64(string)
      get_action_dispatch_file(tmp_file, file_info[:extension]) if tmp_file.present?
    end

    def convert_base64_to_tmp_file(string)
      if string.try(:match, %r{^data:(.*?);(.*?),(.*)$})
        file_info = split_base64(string)
        extension = file_info[:extension]
        binary_data = Base64.decode64(file_info[:data])
        filename = Time.now.to_i.to_s + ".#{extension}"

        tmp_file = Tempfile.new(filename)
        tmp_file.binmode
        tmp_file << binary_data
        tmp_file.rewind
        tmp_file
      else
        nil
      end
    end

    def get_action_dispatch_file(tmp_file, extension)
      filename = Time.now.to_i.to_s + ".#{extension}"
      ActionDispatch::Http::UploadedFile.new(
        :tempfile => tmp_file,
        :filename => filename,
        :type => extension,
      )
    end

    def convert_attachment_to_base64(file_path)
      Base64.encode64(open(file_path) { |io| io.read })
    end

    def attachment_from_url(url)
      return nil if url.blank?
      extname = File.extname(URI.parse(url).path)
      meta_data = "data:image/#{extname.tr('.', '')};base64,"
      string = Base64.encode64(open(url).read)
      string = meta_data + string
      convert_base64_to_attachment(string)
    end

    def get_video_time(path)
      result = `ffmpeg -i #{path} 2>&1 | grep Duration`
      r = result.match("Duration: ([0-9]+):([0-9]+):([0-9]+).([0-9]+)")
      r[1].to_i * 3600 + r[2].to_i * 60 + r[3].to_i if r
    end
  end
end
