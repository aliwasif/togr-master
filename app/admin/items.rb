ActiveAdmin.register Item do
  permit_params :s3_id, :session_id, :status, :recommended, :screenshot

  form do |f|
    f.inputs do
      f.semantic_errors *f.object.errors.keys
      f.input :s3_id, as: :file
      f.input :session_id
      f.input :status
      f.input :recommended
      f.input :screenshot
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    # column :s3_id
    column "Profile Photo" do |item|
      link_to image_tag("https://togrresized.s3-us-west-2.amazonaws.com/#{item.s3_id}"), "https://togr.s3-us-west-2.amazonaws.com/#{item.s3_id}", target: "_blank"
    end
    column :session_id
    column :status
    column :recommended
    column :screenshot
    column :created_at
    actions
  end
end
