ActiveAdmin.register Package do
  permit_params :package_type, :photo, :amount, :worker_remuneration, :expiry,
                :session_length, :photos_taken, :photos_count, :extra_photos_commission,
                :extra_photos_price, :info, :package_number, :details, assets_attributes: [:id, :image, :_destroy]

  form do |f|
    f.inputs do
      f.semantic_errors *f.object.errors.keys
      f.input :package_type, as: :select, collection: Package::TYPES::ALL
      f.input :photo
      f.input :amount
      f.input :worker_remuneration
      f.input :expiry
      f.input :session_length
      f.input :photos_taken
      f.input :photos_count
      f.input :extra_photos_price
      f.input :extra_photos_commission
      f.input :package_number
      f.input :info
      f.input :details
      f.inputs do
        f.has_many :assets, heading: 'Assets', allow_destroy: true, new_record: true do |asset|
          asset.input :image
          asset.input :_destroy, :as=>:boolean, :required => false, :label => 'Remove image'
        end
      end
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :package_type
    column :photo
    column :amount
    column :worker_remuneration
    column :expiry
    column :session_length
    column :photos_taken
    column :extra_photos_price
    column :extra_photos_commission
    column :package_number
    column :info
    column :assets do |service|
      if service.assets.count > 0
        # image_tag service.assets.first.image.url, width: 50
      end
    end
    column :created_at
    actions
  end
end
