ActiveAdmin.register User do
  permit_params :email, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :email
    column :full_name
    column :status do |user|
      User::DescriptiveStatuses::ALL[user.status].titleize
    end
    column "Device" do |user|
      user&.devices&.last&.platform&.titleize
    end
    column :created_at
    column :image do |user|
      # image_tag user.image.url
    end
    actions do |user|
      status = user.approved? ? User::DescriptiveStatuses::DISAPPROVED : User::DescriptiveStatuses::APPROVED
      link_to status.titleize , { action: 'update_status', id: user.id, status: user.approved? ? 0 : 1 }, method: :put
    end
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

  action_item "users" do
    link_to 'Users', admin_users_path
  end
  action_item "workers" do
    link_to 'Partners', admin_users_path(role: :worker)
  end

  collection_action :update_status, method: :put do
    if params[:id]
      user = User.find(params[:id])
      user.update!(status: params[:status])
      user.send_status_notification
    end
    redirect_to admin_users_path
  end

  controller do
    def scoped_collection
      params.dig(:role) == Role::ROLES::WORKER ? super.with_role(Role::ROLES::WORKER) : super
    end
  end
end
