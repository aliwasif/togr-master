ActiveAdmin.register Service do
  permit_params :title_english, :title_arabic, :description_english,
                :description_arabic, :tags, :ancestry,
                assets_attributes: [:id, :image, :_destroy]
  json_editor

  form do |f|
    services = Service.collection_services(service.id)
    f.inputs do
      f.semantic_errors *f.object.errors.keys
      f.input(:ancestry, as: :select, collection: services) if services
      f.input :title_english
      f.input :title_arabic
      f.input :description_english
      f.input :description_arabic
      f.input :tags
      f.inputs do
        f.has_many :assets, heading: 'Assets', allow_destroy: true, new_record: true do |asset|
          asset.input :image
          asset.input :_destroy, :as=>:boolean, :required => false, :label => 'Remove image'
        end
      end
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :title_english
    column :title_arabic
    column :description_english
    column :description_arabic
    column :tags
    column :parent do |service|
      if service&.parent&.id
        link_to service&.parent&.title_english, admin_service_path(service&.parent&.id)
      end
    end
    column :assets do |service|
      if service.assets.count > 0
        image_tag service.assets.first.image.url, width: 50
      end
    end
    column :created_at
    actions
  end
end
