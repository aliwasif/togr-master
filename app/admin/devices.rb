ActiveAdmin.register Device do

  index do
    selectable_column
    id_column
    column :enabled
    column :token
    column :user
    column "Email" do |device|
      device.user.email
    end
    column :created_at
    actions
  end

end
