ActiveAdmin.register BusinessProfile do
  permit_params :status

  form do |f|
    f.inputs do
      f.semantic_errors *f.object.errors.keys
      f.input :status, as: :select, collection: BusinessProfile::STATUSES::ALL
    end
    f.actions
  end
end
