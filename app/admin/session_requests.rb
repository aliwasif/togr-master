ActiveAdmin.register SessionRequest do
  permit_params :user, :worker, :package, :user_location, :worker_location, :status, :payment_method,
                :worker_arrived_at_location, :push_response, :declined_users, :advance, :captured
  # index do
  #   selectable_column
  #   id_column
  #   column :user
  #   column :worker
  #   column :package
  #   column :user_location
  #   column :worker_location
  #   column :status
  #   column :worker_arrived_at_location
  #   column :push_response
  #   # column :payment_method
  #   column :declinedUser
  #   column :advance
  #   column :capture
  #   column :created_at
  #   column :updated_at
  #   actions
  # end
  show do |session_request|
    attributes_table do
      row :user
      row :worker
      row :package
      row "user_location" do
        lat = session_request.user_location["lat"]
        lng = session_request.user_location["lon"]
        if lat != 0 && lng != 0 && lat != "lat" && lng != "lon" && lat != nil && lng != nil
          render 'user_map'
        else
          session_request.user_location
        end
      end
      row "worker_location" do
        lat = session_request.worker_location["lat"]
        lng = session_request.worker_location["lon"]
        if lat != 0 && lng != 0 && lat != "lat" && lng != "lon" && lat != nil && lng != nil
          render 'worker_map'
        else
          session_request.worker_location
        end
      end
      row :status
      row :created_at
      row :updated_at
      row :worker_arrived_at_location
      row :push_response
      # row :payment_method
      row :declined_users
      row :advance
      row :captured

    end
  end
end
# AIzaSyAMy8kqCLsLrHHBsu-oKcvqqophUW_ITXw
# AIzaSyCc1t-bj3lzqzUgQAfy203PUWpIw0QhqzI