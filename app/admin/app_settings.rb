ActiveAdmin.register AppSetting do
  permit_params :name, :value, :description, :setting_type, assets_attributes: [:id, :name, :asset_type, :image, :_destroy]
  json_editor

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name
      f.input :value
      f.input :description
      f.input :setting_type, as: :json
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :name
    column :value
    column :setting_type
    column :description
    column :created_at
    actions
  end
end
