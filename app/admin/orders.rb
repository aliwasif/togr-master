ActiveAdmin.register Order do


  show do |order|
    attributes_table do
      row :user
      row :payment_method
      row :session
      row :total
      row :tip
      row :sub_total
      row :status
      row :created_at
      row :updated_at
      row :worker_package
      row :captured
      row :direct_download
      row "Purchased Photos" do
        ul do
          order.order_items.each do |order_item|
            li class: "session_photo" do
              link_to image_tag("https://togrresized.s3-us-west-2.amazonaws.com/#{order_item.item.s3_id}"), "https://togr.s3-us-west-2.amazonaws.com/#{order_item.item.s3_id}", target: "_blank"
            end
          end
        end
      end
    end
  end
end
