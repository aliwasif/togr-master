ActiveAdmin.register Session do
  permit_params :session_request, :requestable_type, :completed_at,
                :expiry_date, :status, :reason, :session_type, :expired, :screenshot, :purchased_at,
                items_attributes: [:id, :s3_id, :recommended, :status, :_destroy]

  index do
    selectable_column
    id_column
    column :worker
    column :completed_at
    column :expiry_date
    column :status do |session|
      Session::Status::DESCRIPTIVE.key(session.status.to_s.to_i)
    end
    column :session_type
    column :user
    column :user_email do |session|
      session&.user&.email
    end
    column :session_request
    column :expired
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    f.inputs do
      f.semantic_errors *f.object.errors.keys
      # f.input :session_request
      f.input :worker
      f.input :user
      f.input :requestable_type
      f.input :completed_at
      f.input :expiry_date
      f.input :status, as: :select, collection: Session::Status::DESCRIPTIVE
      f.input :reason
      f.input :session_type, as: :select, collection: Session::Types::ALL
      f.input :expired
      f.input :screenshot
      f.input :purchased_at
      f.inputs do
        f.has_many :items, heading: 'Session Images', allow_destroy: true, new_record: true do |item|
          item.input :id
          item.input :s3_id, as: :file
          item.input :recommended
          item.input :status, as: :select, collection: Item::STATUS::ALL
        end
      end
    end
  f.actions
  end

  show do |session|
    attributes_table do
      row :user
      row :user_email do
        session&.user&.email
      end
      row :worker
      row :completed_at
      row :expiry_date
      row :status do
        Session::Status::DESCRIPTIVE.key(session.status.to_i).to_s
      end
      row :reason
      row :session_type
      row :session_request
      row :expired
      row :created_at
      row :updated_at
      row "Added Users" do
        ul class: 'list-style-none' do
          session.users.each do |user|
            li do
              link_to "#{user.email} (#{user.full_name})", admin_user_path(user.id) if user.id != session.user.id
            end
          end
        end
      end
      # row "Session Photos" do
      #   ul do
      #     session.items.each do |item|
      #       li class: "session_photo" do
      #         link_to image_tag("https://togrresized.s3-us-west-2.amazonaws.com/#{item.s3_id}"), "https://togr.s3-us-west-2.amazonaws.com/#{item.s3_id}", target: "_blank"
      #       end
      #     end
      #   end
      # end
      row "Session Photos" do
        if session.items.any?
          table_for session.items do
            column :id
            column "Photo" do |item|
              link_to image_tag("https://togrresized.s3-us-west-2.amazonaws.com/#{item.s3_id}"), "https://togr.s3-us-west-2.amazonaws.com/#{item.s3_id}", target: "_blank", size: "15x15"
            end
            column :recommended
            column :status
          end
        end
      end
      row "Session map" do
        render partial: 'session_map'
        # end
      end
    end
  end
end
