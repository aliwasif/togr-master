ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc { I18n.t("active_admin.dashboard") }

  content title: proc { I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
      # span class: "blank_slate" do
      #   span I18n.t("active_admin.dashboard_welcome.welcome")
      # small I18n.t("active_admin.dashboard_welcome.call_to_action")

      columns do

        column do
          div :class => "panel" do
            h3 "Total Users: " "#{User.where('status = ?', 0).count}"
          end
        end
        column do
          div :class => "panel" do
            h3 "Total Partners: " "#{User.where('status = ?', 1).count}" " Available: " "#{User.where("status = ? AND available = ?", 1, true).count}" " Not available: " "#{User.where("status = ? AND available != ?", 1, true).count}"
          end
        end

      end
      columns do
        column do
            render 'users_map'
          end
        end
    end
  end
end
