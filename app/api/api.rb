require 'grape-swagger'
class API < ::Grape::API
  format :json
  prefix :api

  helpers do
    def current_user
      @current_user ||= User.find(doorkeeper_token[:resource_owner_id])
    end

    def api_response(response, status)
      case response
      when Integer
        status response
      when String
        { data: response, status: status }
      when Hash
        { "data": response, status: status }
      when Net::HTTPResponse
        "#{response.code}: #{response.message}"
      else
        status 400 # Bad request
      end
    end

    def not_found
      api_response({ note: nil }, status(:not_found))
    end

    def image_url(image)
      Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
    end
  end

  mount V1::BaseApi
end
