module V1
  class GrapeBase < ::Grape::API
    class << self
      def api_default_options
        {
          headers: {
            "Authorization": {
              description: 'Validates a user',
              required: true,
            },
          },
        }
      end
    end
  end
end
