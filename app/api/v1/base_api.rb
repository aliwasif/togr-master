require 'doorkeeper/grape/helpers'
module V1
  class BaseApi < ::Grape::API
    prefix 'api'
    version 'v1', using: :path
    format :json
    content_type :json, 'application/json'

    helpers Doorkeeper::Grape::Helpers

    rescue_from :all do |e|
      status, message = ExceptionsHandler.error_format(e)
      ErrorLog.log(e, nil)
      error!({ errors: message, status: status }, status)
    end

    helpers do
      def per_page
        params[:per_page].present? ? params[:per_page].to_s : '5'
      end

      def page
        params[:page].present? ? params[:page].to_s : '1'
      end

      def doorkeeper_render_error_with(error)
        status_code = case error.status
                        when :unauthorized
                          401
                        when :forbidden
                          403
                      end

        error!({ errors: [ { key: error.name, message: error.description }], status: status_code }, status_code, error.headers)
      end
    end

    Grape::Validations.register_validator("equal_to", V1::CustomValidators::EqualTo)

    mount V1::Endpoints::UserEndpoints::Users
    mount V1::Endpoints::UserEndpoints::Assets
    mount V1::Endpoints::UserEndpoints::Registrations
    mount V1::Endpoints::UserEndpoints::Passwords
    mount V1::Endpoints::UserEndpoints::Addresses
    mount V1::Endpoints::UserEndpoints::BusinessProfiles
    mount V1::Endpoints::UserEndpoints::PaymentMethods
    mount V1::Endpoints::Sessions
    mount V1::Endpoints::SessionRequests
    mount V1::Endpoints::Services
    mount V1::Endpoints::Jobs
    mount V1::Endpoints::Orders
    mount V1::Endpoints::Packages
    mount V1::Endpoints::AppSettings
    add_swagger_documentation
  end
end
