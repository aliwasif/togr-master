require 'doorkeeper/grape/helpers'
module V1
  module ExceptionsHandler
    def self.error_format(e)
      case e.class.to_s
      when Grape::Exceptions::ValidationErrors.to_s
        status =  status(:not_acceptable)
        message = e.as_json
      when ArgumentError.to_s
        status =  status(:not_acceptable)
        message = [{ key: 'argument_error', message: e.message }]
      when ActiveRecord::RecordInvalid.to_s
        invalid_record = WPRecordInvalid.new(e.record)
        status =  status(:unprocessable_entity)
        message = invalid_record.as_json
      when ActiveRecord::RecordNotFound.to_s
        status =  status(:not_found)
        message = [{ key: 'not_found', message: I18n.t('not_found') }]
      when Stripe::InvalidRequestError.to_s
        # ((Status 400) (Request req_GLlgnCjbio7Ss7) You cannot use a Stripe token more
        # than once: tok_1D9TJeDsq1NAtjVu5Agg3a8D.)
        # ((Status 400) (Request req_GOU6wAbLcygyuq) No such token: asdasdasd)
        key = e.message.include?('token') ? 'payment_method_token' : e.class.name
        status =  status(:unprocessable_entity)
        message = [{ key: key, message: e.message }]
      else
        status =  status(:internal_server_error)
        message = [{ key: 'internal_server_error', message: e.message }]
      end
      [status, message]
    end

    def self.status(status)
      Rack::Utils.status_code(status)
    end

    class WPRecordInvalid
      attr_reader :errors, :messages

      def initialize(record)
        @errors = record.errors
        @messages = record.errors.messages
      end

      def as_json(**_opts)
        messages.map do |k, v|
          {
            key: k.to_s,
            message: v.first,
          }
        end
      end
    end
  end
end
