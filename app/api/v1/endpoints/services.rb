module V1
  module Endpoints
    class Services < ::V1::GrapeBase

      before do
        # doorkeeper_authorize!
      end

      helpers do
        def default_json_options
          { include: [:ancestors, :assets] }
        end
      end

      resource :services do
        desc 'Get all services.'
        get '/' do
          services = Service.main_services
          api_response({ services: services.as_json(include: [:assets]) }, status(:ok))
        end

        route_param :id do
          desc 'Get a services.'
          get '/' do
            service = Service.find(params[:id])
            api_response({ service: service.as_json(default_json_options) }, status(:ok))
          end

          desc 'Get all child services.'
          get '/sub_services' do
            services = Service.sub_services(params[:id])
            api_response({ services: services.as_json(include: [:assets]) }, status(:ok))
          end
        end
      end
    end
  end
end
