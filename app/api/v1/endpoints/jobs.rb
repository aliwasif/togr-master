module V1
  module Endpoints
    class Jobs < ::V1::GrapeBase

      before do
        doorkeeper_authorize!
      end

      helpers do
        def default_json_options
          { include: [:business_profile, :user] }
        end

        def notification(job, send_method)
          notify = PushNotifications::JobsPushNotificationService.new(current_user, job)
          notify.send(send_method)
        end

        def json_options
          {
            include: {
              user: { include: [:assets] },
              worker: {
                include: {
                  assets: {},
                  business_profile: {
                    include: [:service]
                  }
                }
              }
            }
          }
        end
      end

      resource :jobs do
        desc 'Create a job.'
        params do
          requires :job, type: Hash do
            with(allow_blank: false) do
              requires :estimated_time, type: DateTime
              requires :estimated_started_at, type: DateTime
              requires :estimated_completed_at, type: DateTime
              requires :cost, type: Integer
              requires :detail, type: String
              requires :user_id, type: Integer
              requires :business_profile_id, type: Integer
            end
          end
        end
        post '/' do
          job = Job.new(params[:job])
          job.worker_id = current_user.id
          job.status = Job::STATUSES::CREATED
          job.save!
          notification(job, :send_new_job)
          api_response({ job: job.as_json(json_options) }, status(:created))
        end

        desc 'Get all jobs.'
        get '/' do
          jobs = Job.jobs_by_role(current_user)
          api_response({ jobs: jobs.as_json(json_options) }, status(:ok))
        end

        route_param :id do
          desc 'Get a job.'
          get '/' do
            job = current_user.jobs.find(params[:id])
            api_response({ job: job.as_json(json_options) }, status(:ok))
          end

          desc 'Start a job.'
          get '/started' do
            job = Job.find_by(worker_id: current_user.id, id: params[:id])
            job.status = Job::STATUSES::STARTED
            job.started_at = DateTime.now.utc
            job.save!
            notification(job, :send_start_job)
            api_response({ job: job.as_json(json_options) }, status(:ok))
          end

          desc 'End a job.'
          get '/completed' do
            job = Job.find_by(worker_id: current_user.id, id: params[:id])
            job.status = Job::STATUSES::COMPLETED
            job.completed_at = DateTime.now.utc
            job.save!
            notification(job, :send_end_job)
            api_response({ job: job.as_json(json_options) }, status(:ok))
          end

          desc 'Confirm of start a job.'
          get '/confirm_started' do
            job = current_user.jobs.find(params[:id])
            job.status = Job::STATUSES::CONFIRM_STARTED
            job.save!
            notification(job, :send_confirm_start_job)
            api_response({ job: job.as_json(json_options) }, status(:ok))
          end

          desc 'Confirm of end a job.'
          get '/confirm_completed' do
            job = current_user.jobs.find(params[:id])
            job.status = Job::STATUSES::CONFIRM_COMPLETED
            job.save!
            notification(job, :send_confirm_completed_job)
            api_response({ job: job.as_json(json_options) }, status(:ok))
          end

          desc 'Cancel a job.'
          get '/canceled' do
            job = Job.where(
              "id = ? AND (user_id = ? OR worker_id = ?)",
              params[:id],
              current_user.id,
              current_user.id
            )
            job.status = Job::STATUSES::CANCEL
            job.save!
            role = (current_user.has_role?(Role::ROLES::WORKER) ? "worker" : "user")
            notification(job, "send_cancel_job_by_#{role}".to_sym)
            api_response({ job: job.as_json(json_options) }, status(:ok))
          end

          desc 'Start a job.'
          get '/rejected' do
            job = current_user.jobs.find(params[:id])
            job.status = Job::STATUSES::REJECTED
            job.save!
            notification(job, :send_reject_job)
            api_response({ job: job.as_json(json_options) }, status(:ok))
          end

          desc 'Start a job.'
          get '/accepted' do
            job = current_user.jobs.find(params[:id])
            job.status = Job::STATUSES::ACCEPTED
            job.save!
            notification(job, :send_accept_job)
            api_response({ job: job.as_json(json_options) }, status(:ok))
          end
        end
      end
    end
  end
end
