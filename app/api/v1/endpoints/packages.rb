module V1
  module Endpoints
    class Packages < ::V1::GrapeBase

      before do
        doorkeeper_authorize!
      end

      resource :packages do
        desc 'Get all packages.'
        get '/' do
          packages = Package.all
          api_response({ packages: packages.order( 'package_number DESC' ).as_json(include: :assets) }, status(:ok))
        end
      end
    end
  end
end
