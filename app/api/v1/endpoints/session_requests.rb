module V1
  module Endpoints
    class SessionRequests < ::V1::GrapeBase

      before do
        doorkeeper_authorize!
      end

      resource :session_requests do

        desc 'Send a session request'
        params do
          requires :session_request, type: Hash do
            with(allow_blank: false) do
              requires :package_id, type: Integer
              optional :worker_id, type: Integer
              requires :user_location, type: Hash do
                requires :lat, type: Float
                requires :lon, type: Float
              end
            end
          end
        end
        post '/' do
          session_request = SessionRequest.new(params[:session_request])
          session_request.user = current_user
          session_request.status = SessionRequest::Status::INITIATED
          session_request.save!
          session_request.send_request_push_notification
          api_response({ session_request: session_request }, status(:created))
        end

        route_param :id do
          desc 'Decline a session request.'
          put '/declined' do
            session_request = SessionRequest.find(params[:id])
            raise ArgumentError, I18n.t("session_request.errors.expired") if session_request.expired?

            session_request.status = SessionRequest::Status::DECLINED
            session_request.declined_users << current_user.id
            session_request.declined_users = session_request.declined_users.uniq
            session_request.save!
            session_request.send_request_push_notification
            api_response({ session_request: session_request }, status(:created))
          end

          desc 'Cancel a session request.'
          put '/cancel' do
            session_request = SessionRequest.find(params[:id])
            raise ArgumentError, I18n.t("session_request.errors.expired") if session_request.expired?

            session_request.update!(status: SessionRequest::Status::CANCEL)
            session_request.send_cancel_push_notification
            api_response({ session_request: session_request }, status(:created))
          end

          desc 'Accept a request.'
          params do
            requires :session_request, type: Hash do
              requires :worker_location, type: Hash do
                with(allow_blank: false) do
                  requires :lat, type: Float
                  requires :lon, type: Float
                end
              end
            end
          end
          put '/accept' do
            #todo validate a recipient should be worker
            session_request = SessionRequest.find(params[:id])
            raise ArgumentError, I18n.t("session_request.errors.expired") if session_request.expired?

            session_request.worker = current_user
            session_request.status = SessionRequest::Status::ACCEPTED
            session_request.worker_location = params[:session_request]['worker_location']
            session_request.save!
            session_request.send_accept_push_notification
            api_response({ session_request: session_request }, status(:created))
          end
        end
      end
    end
  end
end
