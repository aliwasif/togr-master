module V1
  module Endpoints
    class Orders < ::V1::GrapeBase

      before do
        doorkeeper_authorize!
      end

      helpers do
        def default_json_options
          {
            include: { order_items: { include: [:item] } }
          }
        end
      end

      resource :users do
        resource :me do
          resource :orders do
            desc "Create Order", api_default_options
            params do
              requires :order, type: Hash do
                requires :session_id, type: Integer
                requires :payment_method_id, type: Integer
                optional :package_id, type: Integer
                requires :sub_total, type: Float
                requires :total, type: Float
                requires :tip, type: Float
                requires :order_items, type: Array
                optional :extra_photos_count, type: Integer
              end
            end
            post '/' do
              items_ids = params[:order].delete('order_items')
              session = Session.find(params[:order]['session_id'])
              payment_method = current_user.payment_methods.find(params[:order]['payment_method_id'])
              order = Order.new(params[:order])
              items = session.items.find(items_ids)
              order_items = items.map{ |si| OrderItem.new(order_id: order.id, item_id: si.id) }

              order.order_items = order_items
              order.user = current_user
              order.worker = session.worker
              order.session = session
              order.payment_method = payment_method
              order.save!
              session.items.where(id: order_items.map(&:item_id)).update_all(status: Item::STATUS::PAID)
              order.capture
              api_response({ order: order.as_json(include: [:package]), reviews: order.session.reviews, session_request: order&.session&.session_request  }, status(:created))
            end

            desc "Orders", api_default_options
            get '/' do
              orders = Order.orders_by_role(current_user)
              api_response(
                {
                  orders: orders.as_json(
                    methods: [:worker_rating, :worker_reviews, :session_package],
                    include: [:session, :worker])
                }, status(:ok))
            end

            route_param :id do
              desc 'Get an order.'
              get '/' do
                order = current_user.orders.find(params[:id])
                api_response(
                  {
                    user: current_user.card,
                    order: order.as_json(
                      include: {
                        order_items: {
                          include: [:item]
                        }
                      }
                    )
                  },
                  status(:ok)
                )
              end

              get '/calculate' do
                order = Order.find(params[:id])
                api_response({ order: order.calculate }, status(:ok))
              end
            end
          end
        end
      end
    end
  end
end
