module V1
  module Endpoints
    class AppSettings < ::V1::GrapeBase
      resource :app_settings do
        before do
          doorkeeper_authorize!
        end

        desc "Get online status", api_default_options
        get '/' do
          api_response({ app_settings: AppSetting.all }, status(:ok))
        end
      end
    end
  end
end
