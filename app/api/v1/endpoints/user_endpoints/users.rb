module V1
  module Endpoints
    module UserEndpoints
      class Users < ::V1::GrapeBase

        helpers do
          def user_location
            current_user.location
          end

          def user_business_profile
            current_user.business_profile
          end

          def default_json_options
            { include:
                {
                  addresses: {},
                  assets: {},
                  devices: {},
                  location: {},
                  roles: {},
                  reviews: { include: { user: { only: [:first_name, :last_name], include: [:assets] } } }
                },
              only: [
                :first_name,
                :last_name,
                :dob,
                :gender,
                :phone_numbers,
                :email,
                :status,
                :id,
                :business_name,
                :business_type,
                :onboarding_state,
                :privacy_settings
              ],
              methods: [:average_rating]
            }
          end
        end

        resource :users do
          desc "Search a worker by uid.", api_default_options
          params do
            requires :user, type: Hash do
              with(allow_blank: false) do
                requires :uid, type: String
              end
            end
          end
          post '/search' do
            user = User.with_role(Role::ROLES::WORKER).where(worker_uid: params[:user]['uid']).first
            api_response({ user: user }, status(:ok))
          end

          before do
            doorkeeper_authorize!
          end

          desc 'Nearby togrs'
          params do
            requires :user_location, type: Hash do
              requires :lat, type: Float
              requires :lon, type: Float
            end
          end
          post '/nearby' do
            firebase = FirebaseServices::GeoService.new(params[:user_location])
            workers = firebase.nearby
            api_response({ nearby_workers: workers }, status(:ok))
          end

          desc "Get online status", api_default_options
          get '/:id/available' do
            user = User.find(params[:id])
            api_response({ available: user.available }, status(:ok))
          end
          resource :me do
            desc "Update user's profile.", api_default_options
            params do
              requires :user, type: Hash do
                with(allow_blank: false) do
                  optional :first_name, type: String
                  optional :last_name, type: String
                  optional :phone_numbers, type: Array
                end
                optional :profile, type: Hash
                optional :gender, type: Integer
              end
            end
            put 'profile' do
              current_user.update!(params[:user])
              current_user.location.save!
              api_response({ user: current_user }, status(:created))
            end

            desc "Update online status", api_default_options
            params do
              requires :user, type: Hash do
                with(allow_blank: false) do
                  requires :available, type: Boolean
                end
              end
            end
            put '/available' do
              current_user.update!(params[:user])
              api_response({ user: current_user }, status(:created))
            end

            put 'update_password' do
              password_updated = current_user.update_with_password(params[:user])
              user = password_updated ? current_user : nil
              api_response({ user: user, password_updated: password_updated }, status(:created))
            end

            desc 'Return a profile.', api_default_options
            get 'profile' do
              api_response({ user: current_user }, status(:ok))
            end

            desc 'Return a user activities.', api_default_options
            get 'activities' do
              activities = PublicActivity::Activity.order("created_at desc").where(owner_id: current_user.id, owner_type: "User")
              api_response({ activities: activities }, status(:ok))
            end

            desc 'Add devices.'
            params do
              requires :device, type: Hash do
                requires :token, type: String, allow_blank: false
                requires :platform, type: String, allow_blank: false
              end
            end
            resource :devices do
              post do
                Device.where(token: params[:device]['token']).destroy_all
                device = current_user.devices.create!(params[:device])
                api_response({ device: device }, status(:created))
              end
            end

            desc 'Add screenshots.'
            params do
              requires :screenshot, type: Hash do
                requires :session_id, type: Integer, allow_blank: false
                requires :item_id, type: Integer, allow_blank: false
              end
            end
            resource :screenshots do
              post do
                screenshot = current_user.screenshots.create!(params[:screenshot])
                screenshot.send_screenshot_taken_notification
                api_response({ screenshot: screenshot }, status(:created))
              end
            end

            desc 'Add reviews.'
            resource :reviews do
              desc 'Add reviews.'
              params do
                requires :review, type: Hash do
                  requires :rating, type: Integer, allow_blank: false
                  requires :worker_id, type: Integer, allow_blank: false
                  requires :session_id, type: Integer, allow_blank: false
                  requires :order_id, type: Integer, allow_blank: false
                  requires :rating, type: Integer, allow_blank: false
                  optional :medal, type: Integer
                  optional :comment, type: String
                end
              end

              post do
                review = Review.new(params[:review])
                review.user_id = current_user.id
                review.save!
                review.send_review_notification
                api_response({ review: review }, status(:created))
              end

              desc 'Get rating.'
              get '/rating' do
                reviews = Review.where(worker_id: current_user.id).order("created_at DESC")
                api_response({ rating: reviews&.average(:rating)&.round(2) }, status(:ok))
              end

              desc 'Get all reviews'
              get '/' do
                reviews = Review.where(worker_id: current_user.id).order("created_at DESC")
                api_response({ reviews: reviews.as_json(include: { user: { include: :assets } } ) }, status(:ok))
              end
            end
          end
        end
      end
    end
  end
end
