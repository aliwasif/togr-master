module V1
  module Endpoints
    module UserEndpoints
      class Passwords < ::V1::GrapeBase
        desc 'Forgot password.'
        params do
          requires :user, type: Hash do
            requires :email, type: String
          end
        end
        post 'forgot_password' do
          user = User.find_by(email: params[:user]['email'])
          if user&.send_reset_password_code
            api_response(
              { message: I18n.t('password.send_reset_password_instructions') },
              status(:created)
            )
          else
            error!({
              errors: [
                { key: 'user', message: "Unauthorized." },
              ], status: status(:unauthorized),
            }, status(:unauthorized))
          end
        end

        desc 'Verfiy reset password code.'
        params do
          requires :user, type: Hash do
            requires :code, type: String
          end
        end
        post 'verify_reset_password_code' do
          original_token = params[:user]['code']
          valid_code = User.valid_reset_password_code?(original_token)
          if valid_code
            api_response({ message: I18n.t('password.valid_code') }, status(:created))
          else
            error!({
              errors: [
                { key: 'code', message: "Invalid verification code." },
              ], status: status(:unauthorized),
            }, status(:unauthorized))
          end
        end

        desc 'Reset password.'
        params do
          requires :user, type: Hash do
            requires :code, type: String
            requires :password, type: String, equal_to: 'password_confirmation'
            requires :password_confirmation, type: String
          end
        end
        post 'reset_password' do
          attributes = {}
          original_token = params[:user]['code']
          attributes[:reset_password_token] = original_token
          attributes[:password] = params[:user]['password']
          attributes[:password_confirmation] = params[:user]['password_confirmation']
          valid_code = User.valid_reset_password_code?(original_token)
          user = valid_code ? User.reset_password_by_token(attributes) : nil
          if user
            api_response(
              { message: I18n.t('password.reset_password_successful') },
              status(:created)
            )
          elsif !valid_code
            error!({
              errors: [
                {
                  key: 'code', message: I18n.t("password.invalid_code"),
                },
              ], status: status(:unauthorized),
            }, status(:unauthorized))
          end
        end
      end
    end
  end
end
