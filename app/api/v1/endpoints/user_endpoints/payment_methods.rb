module V1
  module Endpoints
    module UserEndpoints
      class PaymentMethods < ::V1::GrapeBase
        before do
          doorkeeper_authorize!
        end

        resource :users do
          resource :me do
            resource :payment_methods do
              desc 'Add payment method'
              params do
                requires :payment_method_token, type: String
              end
              post '/' do
                customer = Stripe::UtilityService.customer(current_user)
                stripe_payment_method = customer.sources.create(
                  source: params[:payment_method_token]
                )
                payment_method = current_user.payment_methods.create!(
                  json: stripe_payment_method.as_json
                )
                current_user.set_onboarding_state
                api_response({ payment_method: payment_method }, status(:created))
              end

              get '/' do
                api_response({ payment_methods: current_user.payment_methods }, status(:ok))
              end

              route_param :id do
                put '/default' do
                  payment_methods = current_user.payment_methods
                  payment_method = payment_methods.find(params[:id])
                  payment_methods.each { |pm| pm.update!(primary: false) }
                  payment_method.update!(primary: true)
                  api_response({ payment_method: payment_method }, status(:created))
                end

                desc 'Remove a payment method', api_default_options
                delete '/' do
                  payment_methods = current_user.payment_methods
                  payment_method = payment_methods.find(params[:id])
                  if payment_methods.count > 1 && !payment_method.primary
                    payment_method.destroy
                    api_response({ message: 'Payment method has been deleted successfully.' }, status(:ok))
                  else
                    raise ArgumentError, I18n.t("payment_methods.errors.default")
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
