module V1
  module Endpoints
    module UserEndpoints
      class Transactions < ::V1::GrapeBase
        before do
          doorkeeper_authorize!
        end

        resource :users do
          resource :me do
            resource :transactions do
              desc "Get all transactions", api_default_options
              get '/' do
                tansactions = current_user.transactions
                api_response({ dates: tansactions }, status(:ok))
              end
            end
          end
        end
      end
    end
  end
end
