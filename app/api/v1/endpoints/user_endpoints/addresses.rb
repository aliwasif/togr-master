module V1
  module Endpoints
    module UserEndpoints
      class Addresses < ::V1::GrapeBase
        before do
          doorkeeper_authorize!
        end

        helpers do
          def set_primary_address(primary, addresses, address)
            if primary
              addresses.update_all(primary: false)
              address.update!(primary: true)
            end
          end
        end

        resource :users do
          resource :me do
            resource :addresses do
              desc 'Get all addresses', api_default_options
              get '/' do
                api_response({ addresses: current_user.addresses }, status(:ok))
              end

              desc 'Add an Address', api_default_options
              params do
                requires :address, type: Hash do
                  with(allow_blank: false) do
                    requires :street, type: String
                    requires :city, type: String
                    requires :state, type: String
                    requires :zip, type: String
                  end
                  optional :primary, type: Boolean
                end
              end
              post '/' do
                addresses = current_user.addresses
                addresses.update_all(primary: false) if params[:address]['primary']
                addresses.create!(params[:address])
                current_user.set_onboarding_state
                api_response({ addresses: current_user.addresses }, status(:created))
              end

              route_param :id do
                desc 'Get an Address', api_default_options
                get '/' do
                  address = current_user.addresses.find(params[:id])
                  api_response({ address: address }, status(:ok))
                end

                desc 'Update an Address', api_default_options
                params do
                  requires :address, type: Hash do
                    with(allow_blank: false) do
                      requires :street, type: String
                      requires :city, type: String
                      requires :state, type: String
                      requires :zip, type: String
                    end
                    optional :primary, type: Boolean
                  end
                end
                put '/' do
                  addresses = current_user.addresses
                  address = addresses.find(params[:id])
                  addresses.update_all(primary: false) if params[:address]['primary']
                  address.update!(params[:address])
                  # set_primary_address(params[:address]['primary'], addresses, address)
                  api_response({ addresses: addresses }, status(:created))
                end

                desc 'Set default Address', api_default_options
                params do
                  requires :address, type: Hash do
                    requires :primary, type: Boolean
                  end
                end

                put '/default' do
                  addresses = current_user.addresses
                  address = addresses.find(params[:id])
                  set_primary_address(true, addresses, address)
                  api_response({ address: address }, status(:created))
                end

                desc 'Remove an Address', api_default_options
                delete '/' do
                  addresses = current_user.addresses
                  address = addresses.find(params[:id])
                  if addresses.count > 1 && !address.primary
                    address.destroy
                    current_user.set_onboarding_state
                    api_response({ addresses: current_user.addresses }, status(:no_content))
                  else
                    raise ArgumentError, I18n.t("addresses.errors.default")
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
