module V1
  module Endpoints
    module UserEndpoints
      class Assets < ::V1::GrapeBase
        before do
          doorkeeper_authorize!
        end

        helpers do
          def process_file_uploads(user)
            params[:assets].each do |asset|
              user.assets.build(
                image: asset['image'],
                name: asset['name']
              )
            end
          end
        end

        resource :users do
          resource :me do
            resource :assets do
              desc 'Add assets.'
              params do
                requires :assets, type: Array do
                  with(allow_blank: false) do
                    optional :name, type: String
                    optional :image, type: String
                    requires :s3_id, type: String
                  end
                end
              end
              post do
                current_user.assets.create!(params[:assets])
                api_response({ assets: current_user.assets }, status(:created))
              end

              get '/' do
                api_response({ assets: current_user.assets }, status(:ok))
              end

              route_param :id do
                put '/' do
                  asset = current_user.assets.find(params[:id])
                  asset.update!(params[:asset])
                  api_response({ asset: asset }, status(:created))
                end

                desc 'Remove an asset', api_default_options
                delete '/' do
                  asset = current_user.assets.find(params[:id])
                  asset.destroy
                  api_response({ asset: asset }, status(:no_content))
                end
              end
            end
          end
        end
      end
    end
  end
end
