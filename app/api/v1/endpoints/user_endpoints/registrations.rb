require 'pry'
module V1
  module Endpoints
    module UserEndpoints
      class Registrations < ::V1::GrapeBase
        resource :users do
          desc 'Create a user.'
          params do
            requires :user, type: Hash do
              with(allow_blank: false) do
                requires :email, regexp: /.+@.+/
                requires :first_name, type: String
                requires :last_name, type: String
                requires :gender, type: Boolean
                requires :password, type: String, equal_to: 'password_confirmation'
                requires :password_confirmation, type: String
                requires :user_role, type: String
              end
              optional :assets_attributes, type: Array do
                with(allow_blank: false) do
                  requires :name, type: String
                  requires :image, type: String
                end
              end
              optional :addresses_attributes, type: Array do
                with(allow_blank: false) do
                  requires :street, type: String
                  requires :zip, type: String
                  requires :state, type: String
                  requires :country, type: String
                end
              end
              optional :devices_attributes, type: Array do
                with(allow_blank: false) do
                  requires :token, type: String
                  requires :platform, type: String
                end
              end
              requires :location_attributes, type: Hash do
                with(allow_blank: false) do
                  requires :lat, type: String
                  requires :lng, type: String
                end
              end
            end
          end
          post 'register' do
            user = User.find_by(email: params[:user]['email'])
            if user && !user.confirmed?
              user.reset_password(params[:user]['password'], params[:user]['password_confirmation'])
              user.update!(params[:user].except(:email, :password, :password_confirmation))
              user.send_confirmation_instructions
              api_response({ user: user.as_json }, status(:created))
            else
              user = User.new(params[:user])
              user.onboarding_state = User::OnboardingState::EMAIL_NOT_CONFIRMED
              user.save!
              User.send_confirmation_instructions(params)
            end
            api_response({ user: user.as_json }, status(:created))
          end

          desc 'Create an anonymouse user.'
          params do
            requires :user, type: Hash do
              with(allow_blank: false) do
                requires :email, regexp: /.+@.+/\
              end
              optional :phone_numbers, type: Array
              requires :location_attributes, type: Hash do
                with(allow_blank: false) do
                  requires :lat, type: String
                  requires :lng, type: String
                end
              end
            end
          end
          post 'register/anonymous' do
            # on production it should be Devise.friendly_token.first(8)
            user = User.find_by(email: params[:user]['email'])
            if user.nil?
              generated_password = "12345678"
              user = User.new(params[:user])
              user.user_role = Role::ROLES::USER
              user.password = generated_password
              user.onboarding_state = User::OnboardingState::EMAIL_NOT_CONFIRMED
              user.skip_confirmation_notification!
              user.save!
            end
            api_response({ user: user.as_json }, status(:created))
          end

          desc 'Verify user email.'
          params do
            requires :confirmation_token, type: String, allow_blank: false
          end
          post 'confirmation' do
            user = User.confirm_by_token(params[:confirmation_token])
            if user.confirmed?
              user.onboarding_state = User::OnboardingState::EMAIL_CONFIRMED
              user.save
              api_response({ user: user.as_json }, status(:created))
            else
              raise ArgumentError, I18n.t("confirmation.invalid")
            end
          end

          desc 'Resend confirmation code.'
          params do
            requires :user, type: Hash do
              requires :email, regexp: /.+@.+/, allow_blank: false
            end
          end
          post 'resend_confirmation' do
            user = User.find_by_email!(params[:user]['email'])
            if !user&.confirmed? && user&.send_confirmation_instructions
              api_response({ user: user.as_json }, status(:created))
            else
              raise ArgumentError, I18n.t("confirmation.confirmed")
            end
          end
        end
      end
    end
  end
end
