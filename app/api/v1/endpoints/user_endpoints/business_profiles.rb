module V1
  module Endpoints
    module UserEndpoints
      class BusinessProfiles < ::V1::GrapeBase

        helpers do
          def filtering_params(params)
            params.slice(:service, :location, :title)
          end

          def default_json_options
            { include: { assets: {}, location: {}, worker: { include: [:assets], methods: :average_rating }, service: { include: [:assets] } } }
          end

          def default_json_for_business_profile
            {
              include: {
                assets: {},
                location: {},
                user: {
                  include: {
                    assets: {},
                    reviews: { include: { user: { only: [:first_name, :last_name], include: [:assets] } } }
                  } ,
                  methods: [:average_rating] },
                service: {
                  include: [:assets]
                }
              }
            }
          end
        end

        resource :business_profiles do
          resource :me do

            before do
              doorkeeper_authorize!
            end

            desc "Create a business profile", api_default_options
            params do
              requires :business_profile, type: Hash do
                with(allow_blank: false) do
                  optional :title, type: String
                  optional :service_id, type: Integer
                  optional :description, type: String
                  optional :tags, type: Array[String]
                end
              end
              optional :assets, type: Array do
                with(allow_blank: false) do
                  requires :name, type: String
                  requires :image, type: String
                end
              end
            end
            post '/' do
              business_profile = BusinessProfile.new(params['business_profile'])
              business_profile.user = current_user
              business_profile.assets.build(params[:assets])
              business_profile.location = current_user.location
              business_profile.save!
              api_response({ business_proifle: current_user.business_profile.as_json(include: [:assets, :service, :location]) }, status(:created))
            end

            desc "Update a business profile", api_default_options
            params do
              requires :business_profile, type: Hash do
                with(allow_blank: false) do
                  optional :title, type: String
                  optional :description, type: String
                  optional :tags, type: Array[String]
                end
              end
              optional :assets, type: Array do
                with(allow_blank: false) do
                  requires :name, type: String
                  requires :image, type: String
                end
              end
            end
            put '/' do
              business_profile = current_user.business_profile
              business_profile.update!(params[:business_profile])
              api_response({ business_profile: current_user.business_profile.as_json(include: [:service, :assets]) }, status(:created))
            end

            desc 'Return a business profile.', api_default_options
            get '/' do
              api_response({ business_profile: current_user.business_profile_by_role.as_json(default_json_for_business_profile) }, status(:ok))
            end
          end

          desc 'Return business profiles.', api_default_options
          get '/search' do
            business_profiles = Location.services(params)
            api_response({ business_profiles: business_profiles.as_json(default_json_for_business_profile)}, status(:ok))
          end

          desc 'Return business profiles.', api_default_options
          get '/' do
            business_profiles = BusinessProfile.all
            api_response({ business_profiles: business_profiles.as_json(default_json_for_business_profile)}, status(:ok))
          end

          route_param :id do
            desc 'Return a business profile', api_default_options
            get '/' do
              business_profile = BusinessProfile.find(params[:id])
              api_response({ business_profile: business_profile.as_json(default_json_for_business_profile) }, status(:ok))
            end
          end
        end
      end
    end
  end
end
