module V1
  module Endpoints
    class Sessions < ::V1::GrapeBase

      before do
        doorkeeper_authorize!
      end

      resource :sessions do
        desc 'Start a session'
        params do
          requires :session, type: Hash do
            with(allow_blank: false) do
              requires :session_request, type: Integer
            end
          end
        end
        post '/' do
          session_request = SessionRequest.find(params[:session]['session_request'])
          session = Session.create!(
            user: session_request.user,
            worker: session_request.worker,
            session_request_id: session_request.id,
            session_type: Session::Types::REGULAR
          )
          UserSession.create!(
            user: session_request.user,
            worker: session_request.worker,
            session: session
          )
          session_request.charge_and_capture
          session.send_start_session_notification
          api_response({ session: session }, status(:created))
        end

        desc 'Start an instant session'
        params do
          requires :session, type: Hash do
            with(allow_blank: false) do
              requires :user_id, type: Integer
            end
          end
        end
        post '/instant' do
          user = User.find(params[:session]['user_id'])
          session = Session.create!(
            user: user,
            worker: current_user,
            session_type: Session::Types::INSTANT
          )
          UserSession.create!(
            user: user,
            worker: current_user,
            session: session
          )
          session.send_start_session_notification
          session.user.send_start_session_email
          api_response({ session: session }, status(:created))
        end

        desc 'Get all sessions.'
        get '/' do
          sessions = current_user.sessions_by_filter(params[:filter]).paginate(page: page, per_page: per_page)
          api_response({ sessions: sessions }, status(:ok))
        end

        route_param :id do
          desc 'Get a session.'
          get '/' do
            session = current_user.session_by_role(params[:id])
            api_response({ session: session&.as_json(include: [:items, :orders]) }, status(:ok))
          end

          desc 'Download session paid items.'
          get '/download' do
            session = current_user.sessions.find(params[:id])
            api_response(
              {
                user: current_user.card,
                items: session.items_paid
              },
              status(:ok)
            )
          end

          desc 'complete a session.'
          post '/completed' do
            session = Session.find_by(id: params[:id], worker: current_user)
            session.update!(completed_at: DateTime.now.utc, status: Session::Status::COMPLETED)
            session.send_completed_session_notification
            api_response({ session: session }, status(:created))
          end

          desc 'Update a session by worker.'
          params do
            requires :session, type: Hash do
              with(allow_blank: false) do
                requires :items, type: Array
                requires :recommended_items, type: Array
              end
            end
          end
          put '/' do
            recommended_items = params[:session].delete('recommended_items')
            session = Session.find_by(id: params[:id], worker: current_user)
            items = params[:session]['items']
            session.mark_recommended(items, recommended_items) if recommended_items.any?
            session.send_photos_uploaded_notification
            api_response({ session: session.as_json(include: [:items]) }, status(:created))
          end

          desc 'Add users to a session.'
          params do
            requires :session, type: Hash do
              with(allow_blank: false) do
                requires :emails, type: Array
              end
            end
          end
          post '/add' do
            emails = params[:session].delete('emails')
            users = User.where(email: emails)
            new_users = emails - users.map(&:email)
            User.create_anonymous_users(new_users)
            session = Session.find_by_user_or_worker(params[:id], current_user.id)
            users = User.where(email: emails)
            session.create_user_sessions(users.map(&:id))
            api_response({ session: session.as_json(include: [:items]) }, status(:created))
          end

          desc 'Get all users of a session.'
          get '/users' do
            session = Session.find(params[:id])
            api_response({ session: session.users }, status(:ok))
          end

          desc 'Cancel a request by user or worker.'
          put '/cancel' do
            session = Session.find_by_user_or_worker(params[:id], current_user.id)
            session.status = Session::Status::CANCEL
            session.reason = params[:session]['reason']
            session.save!
            session.send_cancel_session_push_notification
            api_response({ session: session }, status(:created))
          end

          desc 'Arrived at location.'
          params do
            requires :session_request, type: Hash do
              requires :worker_arrived_at_location, type: Hash do
                with(allow_blank: false) do
                  requires :lat, type: Float
                  requires :lon, type: Float
                end
              end
            end
          end
          put '/arrived' do
            #todo validate a recipient should be worker
            session_request = SessionRequest.find(params[:id])
            session_request.status = SessionRequest::Status::ARRIVED
            session_request.worker_arrived_at_location = params[:session_request]['worker_arrived_at_location']
            session_request.save!
            session_request.send_arrived_push_notification
            api_response({ session_request: session_request.as_json }, status(:created))
          end
        end
      end
    end
  end
end
