module V1
  module CustomValidators
    class EqualTo < Grape::Validations::Base
      def validate_param!(attr_name, params)
        unless params[attr_name] == params[@option]
          fail(
            Grape::Exceptions::Validation,
            params: [@scope.full_name(attr_name)],
            message: I18n.t(
              "not_match",
              attr_name_a: attr_name.to_s.humanize,
              attr_name_b: @option.humanize
            )
          )
        end
      end
    end
  end
end
