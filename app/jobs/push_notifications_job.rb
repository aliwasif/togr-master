class PushNotificationsJob < ApplicationJob
  @queue = :push_notifications

  def self.perform(device_tokens, data)
    PushNotifications::PushNotificationService.new(device_tokens, data).call
  end
end
