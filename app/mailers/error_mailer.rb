class ErrorMailer < ActionMailer::Base
  default from: "info@togr.com"

  def send_error_trace(data)
    @data = data
    mail(to: 'for.apps.testing@gmail.com', subject: 'Error Details')
  end
end
