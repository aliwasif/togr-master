RSpec.configure do |config|
  config.before(:each) do
    DatabaseCleaner.strategy = :transaction
  end

  config.append_after(:each) do
    DatabaseCleaner.clean_with(:truncation)
  end
end
