RSpec.configure do |config|
  config.include Spec::Support::HelperMethods, type: :request
  config.include Spec::Support::HelperMethods, type: :model
end
