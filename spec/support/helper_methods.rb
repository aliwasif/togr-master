module Spec
  module Support
    module HelperMethods
      def self.included(klass)
        klass.class_eval do
          include AttachTestFile
        end
      end

      def parsed_response
        JSON.parse(response.body)
      end

      def parsed_user
        parsed_response['data']['user']
      end

      def response_contains_error?(key: nil, message: nil)
        error_found = false
        parsed_response['errors'].each do |error|
          if key.present? && message.present?
            if error['key'] == key && error['message'] == message
              error_found = true
            end
          elsif key.present?
            error_found = true if error['key'] == key
          elsif message.present?
            error_found = true if error['message'] == message
          end
        end
        error_found
      end

      def authorization_header(token)
        {
          'Authorization' =>  "Bearer #{token.token}",
          'Content-Type' => 'application/json',
        }
      end

      def test_audio_note_base64
        base64 = Base64.encode64(
          File.open(Rails.root.join('spec', 'support', 'files', 'test_audio.aac')).read
        )
        "data:audio/aac;base64,#{base64}"
      end

      def test_image_base64
        base64 = Base64.encode64(
          File.open(Rails.root.join('spec', 'support', 'files', 'test_image.jpeg')).read
        )
        "data:image/jpeg;base64,#{base64}"
      end

      def test_video_base64
        base64 = Base64.encode64(
          File.open(Rails.root.join('spec', 'support', 'files', 'test-video.MP4')).read
        )
        "data:video/MP4;base64,#{base64}"
      end

      def oauth_token(application, user)
        FactoryGirl.create(
          :access_token,
          :application => application,
          :resource_owner_id => user.id
        )
      end

      def create_friendship(user, other_user)
        FactoryGirl.create(:friendship, user: user, friend: other_user,
                                        status: Friendship::Status::ACCEPTED)
        Friendship.accept!(other_user.id, user.id)
      end

      def create_friend_request(sender, receiver)
        sender.send_friend_request(receiver.id)
      end

      def chop_year_from_date(date)
        date.to_s.split("-").reject { |item| item.length == 4 }.join("-")
      end

      def send_gift(sender:, recipient:, thank_you: true)
        recipient_address = FactoryGirl.create(:address, user: recipient)
        sender_payment_method = FactoryGirl.create(:payment_method, user: sender)
        recipient_occasion = FactoryGirl.create(:occasion, user: recipient)
        order = FactoryGirl.create(:order,
                                   user: sender, recipient: recipient, address: recipient_address,
                                   payment_method: sender_payment_method, delivery_date: Date.today,
                                   occasion: recipient_occasion)
        order.post_thank_you_note("Thank you so much friend!") if thank_you
        vendor = FactoryGirl.create(:vendor, name: Vendors::FloristOne::FloristOneService.vendor_name)
        sub_order = FactoryGirl.create(:sub_order, vendor: vendor, order: order)
        vendor_item = FactoryGirl.create(:flower, vendor: vendor)
        sub_order.order_items.create!(vendor_item: vendor_item)
      end
    end
  end
end
