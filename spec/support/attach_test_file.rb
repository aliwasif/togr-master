module Spec
  module Support
    module AttachTestFile
      def attach_test_file(model, attribute_name)
        model.public_send(attribute_name).
          attach(
            io: File.open(Rails.root.join('db', 'seed_data', 'images', 'card1-thumb.png')),
            filename: 'card1-thumb.png',
            content_type: 'image/png'
          )
      end

      def attach_video_file(model, attribute_name)
        model.public_send(attribute_name).
          attach(
            io: File.open(Rails.root.join('db', 'seed_data', 'videos', 'test-video.MP4')),
            filename: 'test-video.MP4',
            content_type: 'video/MP4'
          )
      end
    end
  end
end
