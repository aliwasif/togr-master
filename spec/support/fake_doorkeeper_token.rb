class FakeDoorkeeperToken < Hash
  def initialize(resource_owner)
    self[:resource_owner_id] = resource_owner.id
    self[:expired?] = false
    self[:accessible?] = true
  end

  def acceptable?(arg)
    true
  end
end
