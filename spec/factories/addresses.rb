FactoryGirl.define do
  factory :address do
    street "jail road"
    city "Lahore"
    state "Punjab"
    zip 54000
    primary true
  end
end
