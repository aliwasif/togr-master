require 'rails_helper'
require 'spec_helper'

describe V1::Endpoints::UserEndpoints::Addresses do
  include GrapeRouteHelpers::NamedRouteMatcher

  let(:user) { FactoryGirl.create(:user) }
  let(:user_b) { FactoryGirl.create(:user) }
  let(:application) { FactoryGirl.create(:application) }
  let(:request_body) do
    {
      "address": {
        "state": "Punjab",
        "city": "Lahore",
        "street": "Star st",
        "zip": "54000",
        "primary": true,
      },
    }
  end

  context 'Positive Cases: ' do
    it 'Verify that "Add an Address" API returns status (201 - created) ' \
    'upon sending a POST request' do
      token = oauth_token(application, user)
      post(
        api_v1_users_me_addresses_path,
        params: request_body.to_json,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:created)
      address = parsed_response['data']['addresses']
      validate_address(address[0])
    end

    it 'Verify that "Add an Address" API returns status (201 - created) ' \
    'upon sending a POST request with Primary value as false' do
      request_body = {
        "address": {
          "state": "Punjab",
          "city": "Lahore",
          "street": "Jail Road",
          "zip": "54000",
          "primary": false,
        },
      }
      token = oauth_token(application, user)
      post(
        api_v1_users_me_addresses_path,
        params: request_body.to_json,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:created)
    end

    it 'Verify that "Add Default Address" API returns (True) upon adding a DEFAULT address' do
      token = oauth_token(application, user)
      request_body = {
        "address": {
          "state": "Punjab",
          "city": "Lahore",
          "street": "Star st",
          "zip": "54000",
          "primary": false,
        },
      }

      post(
        api_v1_users_me_addresses_path,
        params: request_body.to_json,
        headers: authorization_header(token)
      )

      put(
        "/api/v1/users/me/addresses/#{user.addresses.first.id}/default",
        headers: authorization_header(token),
        params: request_body.to_json
      )
      expect(user.addresses.first.primary).to be_truthy
    end

    it 'Verify that "Get an Address" API returns status (200 - ok) upon sending a GET request' do
      token = oauth_token(application, user)
      address = create_address(user)
      get(
        api_v1_users_me_addresses_path(id: address.id),
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:ok)
      address = parsed_response['data']['address']
      validate_address(address)
    end

    it 'Verify that "Get all User Addresses" API returns status (200 - ok) ' \
    'upon sending a GET request' do
      token = oauth_token(application, user)
      create_address(user)
      get(
        api_v1_users_me_addresses_path,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:ok)
      address = parsed_response['data']['addresses']
      validate_address(address[0])
    end

    it 'Verify that "Remove an Address" API returns status (204 - no_content) ' \
    'upon deleting a NOT DEFAULT address' do
      address_a = create_address(user)
      address_b = create_address(user)

      token = oauth_token(application, user)
      put(
        api_v1_users_me_addresses_default_path(id: address_a.id),
        headers: authorization_header(token),
        params: request_body.to_json
      )
      expect(response).to have_http_status(:created)
      delete(
        api_v1_users_me_addresses_path(id: address_b.id),
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:no_content)
    end

    it 'Verify that "Update an Address" API returns status (201 - created) ' \
    'upon sending a PUT request' do
      token = oauth_token(application, user)
      address = create_address(user)
      put(
        api_v1_users_me_addresses_path(id: address.id),
        params: request_body.to_json,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:created)
    end
  end

  context 'Negative Cases: ' do
    it 'Verify that "Add an Address" API returns status (406 - not_acceptable) ' \
    'upon sending a POST request without request body' do
      token = oauth_token(application, user)
      post(
        api_v1_users_me_addresses_path,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_acceptable)
    end

    it 'Verify that "Add an Address" API returns status (401 - unauthorized) upon ' \
    ' sending a POST request without authorization token' do
      post(
        api_v1_users_me_addresses_path,
        params: request_body.to_json
      )
      expect(response).to have_http_status(:unauthorized)
    end

    it 'Verify that "Add an Address" API returns status (406 - not_acceptable) upon ' \
    'sending a POST request with empty values' do
      request_body = {
        "address": {
          "state": "",
          "city": "",
          "street": "",
          "zip": "",
          "primary": true,
        },
      }
      token = oauth_token(application, user)
      post(
        api_v1_users_me_addresses_path,
        params: request_body.to_json,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_acceptable)
    end

    it 'Verify that "Add an Address" API returns status (406 - not_acceptable) upon ' \
    'sending a POST request with missing Keys' do
      request_body = {
        "address": {
          "state": "Punjab",
          "city": "Lahore",
          "street": "jail road",
          "primary": true,
        },
      }
      token = oauth_token(application, user)
      post(
        api_v1_users_me_addresses_path,
        params: request_body.to_json,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_acceptable)
    end

    it 'Verify that "Get an Address" API returns status (404 - not_found) upon ' \
    'sending a GET request with Bogus address ID' do
      token = oauth_token(application, user)
      get(
        api_v1_users_me_addresses_path(id: 88888),
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_found)
    end

    it 'Verify that "Get an Address" API returns status (401 - unauthorized) ' \
    'upon sending a GET request without token' do
      address = create_address(user)
      get(
        api_v1_users_me_addresses_path(id: address.id),
      )
      expect(response).to have_http_status(:unauthorized)
    end

    it 'Verify that "Get an Address" API returns status (404 - not_found) upon creating address' \
    'User-A & by getting that address from User-b' do
      address_a = create_address(user)
      token = oauth_token(application, user_b)
      get(
        api_v1_users_me_addresses_path(id: address_a.id),
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_found)
    end

    it 'Verify that "Remove an Address" API returns status (404 - not_found) upon ' \
    'sending a DELETE request with Bogus address ID' do
      token = oauth_token(application, user)
      delete(
        api_v1_users_me_addresses_path(id: 88888),
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_found)
    end

    it 'Verify that "Remove an Address" API returns status (401 - unauthorized) ' \
    'upon sending a DELETE request without token' do
      address = create_address(user)
      delete(
        api_v1_users_me_addresses_path(id: address.id),
      )
      expect(response).to have_http_status(:unauthorized)
    end

    it 'Verify that "Remove an Address" API returns status (406 - not_acceptable) ' \
    'upon deleting a DEFAULT address' do
      token = oauth_token(application, user)
      request_body = {
        "address": {
          "state": "Punjab",
          "city": "Lahore",
          "street": "Star st",
          "zip": "54000",
          "primary": false,
        },
      }

      post(
        api_v1_users_me_addresses_path,
        params: request_body.to_json,
        headers: authorization_header(token)
      )

      post(
        api_v1_users_me_addresses_path,
        params: request_body.to_json,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:created)

      put(
        "/api/v1/users/me/addresses/#{user.addresses.first.id}/default",
        headers: authorization_header(token),
        params: request_body.to_json
      )
      expect(response).to have_http_status(:created)

      delete(
        api_v1_users_me_addresses_path(id: user.addresses.first.id),
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_acceptable)
    end

    it 'Verify that "Add default Address" API returns status (406 - not_acceptable) ' \
    'upon adding 2 DEFAULT addresses' do
      token = oauth_token(application, user)
      request_body = {
        "address": {
          "state": "Punjab",
          "city": "Lahore",
          "street": "Star st",
          "zip": "54000",
          "primary": false,
        },
      }

      post(
        api_v1_users_me_addresses_path,
        params: request_body.to_json,
        headers: authorization_header(token)
      )

      post(
        api_v1_users_me_addresses_path,
        params: request_body.to_json,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:created)

      put(
        "/api/v1/users/me/addresses/#{user.addresses.first.id}/default",
        headers: authorization_header(token),
        params: request_body.to_json
      )

      put(
        "/api/v1/users/me/addresses/#{user.addresses.last.id}/default",
        headers: authorization_header(token),
        params: request_body.to_json
      )
      expect(user.addresses.first.primary).to be_falsey
    end

    it 'Verify that "Add default Address" API returns status (401 - unauthorized) ' \
    'upon sending a PUT request without token' do
      put(
        "/api/v1/users/me/addresses/#{8888}/default",
        params: request_body.to_json
      )
      expect(response).to have_http_status(:unauthorized)
    end

    it 'Verify that "Update an Address" API returns status (404 - not_found) upon ' \
    'sending a PUT request with Bogus address ID' do
      token = oauth_token(application, user)
      put(
        api_v1_users_me_addresses_path(id: 8888),
        params: request_body.to_json,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_found)
    end

    it 'Verify that "Update an Address" API returns status (406 - not_acceptable) upon ' \
    'sending a PUT request with empty values' do
      address = create_address(user)
      request_body = {
        "address": {
          "state": "",
          "city": "",
          "street": "",
          "zip": "",
          "primary": true,
        },
      }
      token = oauth_token(application, user)
      put(
        api_v1_users_me_addresses_path(id: address.id),
        params: request_body.to_json,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_acceptable)
    end

    it 'Verify that "Update an Address" API returns status (404 - not_found) ' \
    'upon updating an address of User-B from User-A' do
      address = create_address(user_b)
      token = oauth_token(application, user)
      put(
        api_v1_users_me_addresses_path(id: address.id),
        params: request_body.to_json,
        headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_found)
    end
  end

  def create_address(user)
    FactoryGirl.create(:address, user: user)
  end

  def validate_address(address)
    expect(address['id']).to be_present
    expect(address['street']).to be_present
    expect(address['city']).to be_present
    expect(address['state']).to be_present
    expect(address['zip']).to be_present
    expect(address['primary']).to be_present
    expect(address['user_id']).to be_present
    expect(address['created_at']).to be_present
    expect(address['updated_at']).to be_present
  end
end
