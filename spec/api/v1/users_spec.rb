require 'rails_helper'
require 'spec_helper'

describe V1::Endpoints::UserEndpoints::Users do
  include GrapeRouteHelpers::NamedRouteMatcher

  let!(:application) { FactoryGirl.create(:application) }
  let!(:user) { FactoryGirl.create(:user) }
  let!(:simple_user) { FactoryGirl.create(:simple_user) }

  context 'Positive Cases: ' do
    context 'GET /api/v1/users/me/profile' do
      it 'verifies simple profile and returns HTTP status 200 - OK' do
        token = oauth_token(application, simple_user)
        get api_v1_users_me_profile_path, headers: authorization_header(token)
        expect(response).to have_http_status(:ok)
        profile = parsed_response['data']['user']
        validate_simple_user_profile(profile)
      end

      it 'verifies full profile and returns HTTP status 200 - OK' do
        token = oauth_token(application, user)
        get api_v1_users_me_profile_path, headers: authorization_header(token)
        expect(response).to have_http_status(:ok)
        profile = parsed_response['data']['user']
        validate_user_profile(profile)
      end

      it 'Verify that "Get Profile" API returns status (200 - ok) upon sending a GET request' do
        token = oauth_token(application, user)
        get(
          api_v1_users_me_profile_path,
          headers: authorization_header(token)
        )
        expect(response).to have_http_status(:ok)
        profile = parsed_response['data']['user']
        validate_user_profile(profile)
      end

      it 'Verify that "Update Profile" API returns status (201 - created) ' \
      'upon sending a GET request' do
        request_body = {
          "user": {
            "first_name": "Hasan",
            "last_name": "Naeem",
            "gender": 1,
            "phone_number": "+923464994996",
            "dob": DateTime.now.utc.to_s,
            "base_64_image": "",
          },
        }
        token = oauth_token(application, user)
        put(
          api_v1_users_me_profile_path,
          params: request_body.to_json,
          headers: authorization_header(token)
        )
        expect(response).to have_http_status(:created)
        profile = parsed_response['data']['user']
        expect(profile['id']).to be_present
        expect(profile['first_name']).to be_present
        expect(profile['last_name']).to be_present
        expect(profile['dob']).to be_present
        expect(profile['gender']).to eq(1)
        expect(profile['phone_number']).to be_present
        expect(profile['email']).to be_present
        expect(profile['onboarding_state']).to eq(2)
        expect(profile['image']).to be_nil
        expect(profile['occasions']).to be_present
        expect(profile['addresses']).to be_empty
      end

      it 'Verify that updating User password is successfully' do
        token = oauth_token(application, user)
        request_body = {
          "user": {
            "current_password": user.password,
            "password": "admin321",
            "password_confirmation": "admin321",
          },
        }
        put(
          api_v1_users_me_update_password_path,
          params: request_body.to_json,
          headers: authorization_header(token)
        )
        expect(parsed_response['data']['password_updated']).to be(true)
      end
    end

    context 'Negative Cases: ' do
      it 'returns HTTP status 401 - Unauthorized' do
        get api_v1_users_me_profile_path
        expect(response).to have_http_status(:unauthorized)
      end

      it 'Verify that updating User password is not successful upon password mismatch' do
        token = oauth_token(application, user)
        request_body = {
          "user": {
            "current_password": user.password,
            "password": "admin3212",
            "password_confirmation": "admin321",
          },
        }
        put(
          api_v1_users_me_update_password_path,
          params: request_body.to_json,
          headers: authorization_header(token)
        )
        expect(response).to have_http_status(:not_acceptable)
        message = ''
        parsed_response['errors'].each do |error|
          message = error['message']
        end
        expect(message).to eq("Password doesn't match with Password confirmation")
      end

      it 'Verify that updating User Password is not successful upon wrong current password' do
        token = oauth_token(application, user)
        request_body = {
          "user": {
            "current_password": "0000",
            "password": "admin321",
            "password_confirmation": "admin321",
          },
        }
        put(
          api_v1_users_me_update_password_path,
          params: request_body.to_json,
          headers: authorization_header(token)
        )
        expect(parsed_response['data']['password_updated']).to be(false)
      end

      it 'Verify that updating User Password is not successful upon ' do
        token = oauth_token(application, user)
        request_body = {
          "user": {
            "current_password": "0000",
            "password": "admin321",
            "password_confirmation": "admin321",
          },
        }
        put(
          api_v1_users_me_update_password_path,
          params: request_body.to_json,
          headers: authorization_header(token)
        )
        expect(parsed_response['data']['password_updated']).to be(false)
      end
    end

    def validate_user_profile(profile)
      expect(profile['id']).to be_present
      expect(profile['first_name']).to be_present
      expect(profile['last_name']).to be_present
      expect(profile['dob']).to be_present
      expect(profile['gender']).to be_nil
      expect(profile['phone_number']).to be_present
      expect(profile['email']).to be_present
      expect(profile['onboarding_state']).to be_zero
      expect(profile['image']).to be_nil
      expect(profile['occasions']).to be_empty
      expect(profile['addresses']).to be_empty
    end

    def validate_simple_user_profile(profile)
      expect(profile['id']).to be_present
      expect(profile['email']).to be_present
      expect(profile['onboarding_state']).to be_zero
      expect(profile['first_name']).not_to be_present
      expect(profile['last_name']).not_to be_present
      expect(profile['phone_number']).not_to be_present
      expect(profile['image']).not_to be_present
      expect(profile['dob']).not_to be_present
      expect(profile['gender']).to be_nil
      expect(profile['occasions']).to be_empty
      expect(profile['addresses']).to be_empty
    end
  end
end
