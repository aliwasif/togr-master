require 'rails_helper'
require 'spec_helper'

describe V1::Endpoints::Vendors::Cities do
  include GrapeRouteHelpers::NamedRouteMatcher
  let(:user) { FactoryGirl.create(:user) }
  let(:token) { FakeDoorkeeperToken.new(user) }
  let(:vendor) { FactoryGirl.create(:vendor, name: Vendors::PulseExperiential::PulseExperientialService.vendor_name) }
  let(:vendor_country) { FactoryGirl.create(:vendor_country, vendor: vendor) }
  let(:vendor_state) { FactoryGirl.create(:vendor_state, country: vendor_country) }
  let(:headers) do
    {
      'Authorization':  "Bearer faketoken",
      'Content-Type': 'application/json',
    }
  end

  before(:each) do
    allow_any_instance_of(Doorkeeper::Grape::Helpers).to receive(:doorkeeper_token) { token }
  end

  context 'GET /api/v1/countries/:country_id/cities' do
    it 'functions properly' do
      FactoryGirl.create_list(:vendor_city, 3, country: vendor_country, state: nil)
      get "/api/v1/countries/#{vendor_country.id}/cities", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['cities'].count).to eql(3)
      parsed_response['data']['cities'].each do |state_object|
        expect(state_object['vendor_country_id']).to eql(vendor_country.id)
        expect(state_object['vendor_state_id']).to be nil
      end
    end
  end

  context 'GET /api/v1/countries/:country_id/states/:state_id/cities' do
    it 'functions properly' do
      FactoryGirl.create_list(:vendor_city, 3, country: vendor_country, state: vendor_state)
      get "/api/v1/countries/#{vendor_country.id}/states/#{vendor_state.id}/cities",
          headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['cities'].count).to eql(3)
      parsed_response['data']['cities'].each do |state_object|
        expect(state_object['vendor_country_id']).to eql(vendor_country.id)
        expect(state_object['vendor_state_id']).to eql(vendor_state.id)
      end
    end
  end
end
