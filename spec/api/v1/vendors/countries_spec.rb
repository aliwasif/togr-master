require 'rails_helper'
require 'spec_helper'

describe V1::Endpoints::Vendors::Countries do
  include GrapeRouteHelpers::NamedRouteMatcher
  let(:user) { FactoryGirl.create(:user) }
  let(:token) { FakeDoorkeeperToken.new(user) }
  let(:vendor) { FactoryGirl.create(:vendor, name: Vendors::PulseExperiential::PulseExperientialService.vendor_name) }
  let(:headers) do
    {
      'Authorization':  "Bearer faketoken",
      'Content-Type': 'application/json',
    }
  end

  before(:each) do
    allow_any_instance_of(Doorkeeper::Grape::Helpers).to receive(:doorkeeper_token) { token }
  end

  context 'GET /api/v1/countries' do
    before(:each) do
      FactoryGirl.create_list(:vendor_country, 3, vendor: vendor)
    end

    it 'functions properly' do
      get "/api/v1/countries", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['countries'].count).to eql(3)
      parsed_response['data']['countries'].each do |country_object|
        expect(country_object['vendor_id']).to eql(vendor.id)
      end
    end
  end
end
