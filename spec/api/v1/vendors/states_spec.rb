require 'rails_helper'
require 'spec_helper'

describe V1::Endpoints::Vendors::States do
  include GrapeRouteHelpers::NamedRouteMatcher
  let(:user) { FactoryGirl.create(:user) }
  let(:token) { FakeDoorkeeperToken.new(user) }
  let(:vendor) { FactoryGirl.create(:vendor, name: Vendors::PulseExperiential::PulseExperientialService.vendor_name) }
  let(:vendor_country) { FactoryGirl.create(:vendor_country, vendor: vendor) }
  let(:headers) do
    {
      'Authorization':  "Bearer faketoken",
      'Content-Type': 'application/json',
    }
  end

  before(:each) do
    allow_any_instance_of(Doorkeeper::Grape::Helpers).to receive(:doorkeeper_token) { token }
  end

  context 'GET /api/v1/countries/:country_id/states' do
    it 'functions properly when states exist' do
      FactoryGirl.create_list(:vendor_state, 3, country: vendor_country)
      get "/api/v1/countries/#{vendor_country.id}/states", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['states'].count).to eql(3)
      parsed_response['data']['states'].each do |state_object|
        expect(state_object['vendor_country_id']).to eql(vendor_country.id)
      end
    end

    it 'functions properly when states does not exist' do
      get "/api/v1/countries/#{vendor_country.id}/states", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['states'].count).to be_zero
    end
  end
end
