require 'rails_helper'
require 'spec_helper'

describe V1::Endpoints::Vendors::GiftCards do
  include GrapeRouteHelpers::NamedRouteMatcher
  let(:user) { FactoryGirl.create(:user) }
  let(:token) { FakeDoorkeeperToken.new(user) }
  let(:vendor) { FactoryGirl.create(:vendor, name: Vendors::EGifter::EGifterService.vendor_name) }
  let(:headers) do
    {
      'Authorization':  "Bearer faketoken",
      'Content-Type': 'application/json',
    }
  end

  before(:each) do
    allow_any_instance_of(Doorkeeper::Grape::Helpers).to receive(:doorkeeper_token) { token }
  end

  context 'GET /api/v1/gift_cards' do
    before(:each) do
      FactoryGirl.create_list(:gift_card, 15, vendor: vendor)
    end

    it 'responds with gift cards which match search term' do
      names = ['carvajal', 'carvalho', 'car', 'dacar', 'cicaro']
      names.each do |name|
        FactoryGirl.create(:gift_card, vendor: vendor, name: name)
      end
      get "/api/v1/gift_cards?query=car", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['gift_cards'].count).to eql(5)
      expect(vendor.items.count).to eql(20)
      response_gift_card_ids = []
      parsed_response['data']['gift_cards'].each do |gift_card_object|
        expect(gift_card_object['name']).to include('car')
        response_gift_card_ids << gift_card_object['id']
      end
      vendor.items.where.not(id: response_gift_card_ids).each do |gift_card|
        expect(gift_card.name).not_to include('car')
      end
    end

    it 'reponds with list of gift cards' do
      get '/api/v1/gift_cards', headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '1'
      expect(parsed_response['data']['gift_cards'].count).to be <= 5

      get '/api/v1/gift_cards?page=2', headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '2'
      expect(parsed_response['data']['gift_cards'].count).to be <= 5

      get '/api/v1/gift_cards?page=3', headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '3'
      expect(parsed_response['data']['gift_cards'].count).to be <= 5

      get '/api/v1/gift_cards?page=4', headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '4'
      expect(parsed_response['data']['gift_cards'].count).to be_zero
    end

    it 'responds accurately with the per_page filter' do
      get '/api/v1/gift_cards?per_page=9', headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '1'
      expect(parsed_response['data']['gift_cards'].count).to be <= 9
    end
  end

  context 'GET /api/v1/gift_cards/by_category' do
    before(:each) do
      categories = FactoryGirl.create_list(:vendor_category, 5, vendor: vendor)
      vendor_items = FactoryGirl.create_list(:gift_card, 25, vendor: vendor)
      vendor_items.each do |vendor_item|
        vendor_item.categories = categories.sample(rand(1..3))
      end
    end

    it 'responds with the list of gift_cards grouped by category' do
      get '/api/v1/gift_cards/by_category', headers: headers
      expect(response).to be_successful

      expect(parsed_response['data']['categories'].count).to eql(vendor.categories.count)
      parsed_response['data']['categories'].each do |category_object|
        category = vendor.categories.find(category_object['id'])
        category_gift_card_ids = category.items.pluck(:id)
        category_gift_cards_count = category_gift_card_ids.count
        expect(category_gift_cards_count).to eql(category_object['gift_cards_count'])

        category_object['gift_cards'].each do |gift_card|
          expect(category_gift_card_ids).to include(gift_card['id'])
        end

        expect(category_object['gift_cards'].count).to be <= 5
      end
    end
  end

  context 'GET /api/v1/categories/:category_id/gift_cards' do
    let(:category) { FactoryGirl.create(:vendor_category, vendor: vendor) }

    before(:each) do
      gift_cards = FactoryGirl.create_list(:gift_card, 10, vendor: vendor)
      gift_cards.each { |gift_card| gift_card.categories = [category] }
    end

    it 'responds with gift cards of a single category with pagination' do
      get "/api/v1/categories/#{category.id}/gift_cards?page=1", headers: headers
      expect(response).to be_successful
      category_gift_card_ids = category.items.pluck(:id)
      page1_gift_cards = parsed_response['data']['gift_cards']
      page1_gift_cards.each do |gift_card|
        expect(category_gift_card_ids).to include(gift_card['id'])
      end
      expect(parsed_response['data']['page']).to eql '1'
      expect(parsed_response['data']['gift_cards'].count).to be <= 5

      get "/api/v1/categories/#{category.id}/gift_cards?page=2", headers: headers
      expect(response).to be_successful
      page2_gift_cards = parsed_response['data']['gift_cards']
      expect(page2_gift_cards).not_to eql(page1_gift_cards)
      page2_gift_cards.each do |gift_card|
        expect(category_gift_card_ids).to include(gift_card['id'])
      end
      expect(parsed_response['data']['page']).to eql '2'
      expect(parsed_response['data']['gift_cards'].count).to be <= 5

      get "/api/v1/categories/#{category.id}/gift_cards?page=3", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '3'
      expect(parsed_response['data']['gift_cards'].count).to be_zero
    end
  end
end
