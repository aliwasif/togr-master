require 'rails_helper'
require 'spec_helper'

describe V1::Endpoints::Vendors::Cities do
  include GrapeRouteHelpers::NamedRouteMatcher
  let(:user) { FactoryGirl.create(:user) }
  let(:token) { FakeDoorkeeperToken.new(user) }
  let(:vendor_country) { FactoryGirl.create(:vendor_country, vendor: vendor) }
  let(:vendor_state) { FactoryGirl.create(:vendor_state, country: vendor_country) }
  let(:headers) do
    {
      'Authorization':  "Bearer faketoken",
      'Content-Type': 'application/json',
    }
  end

  before(:each) do
    allow_any_instance_of(Doorkeeper::Grape::Helpers).to receive(:doorkeeper_token) { token }
  end

  context 'GET /api/v1/best_sellers' do
    it 'functions properly' do
      pulse = FactoryGirl.create(:vendor, name: Vendors::PulseExperiential::PulseExperientialService.vendor_name)
      pulse_best_seller_items.each do |item|
        FactoryGirl.create(:vendor_item, name: item, vendor: pulse)
      end
      FactoryGirl.create_list(:vendor_item, 5, vendor: pulse)
      florist = FactoryGirl.create(:vendor, name: Vendors::FloristOne::FloristOneService.vendor_name)
      florist_best_seller_items.each do |item|
        FactoryGirl.create(:vendor_item, name: item, vendor: florist)
      end
      FactoryGirl.create_list(:vendor_item, 5, vendor: florist)
      e_gifter = FactoryGirl.create(:vendor, name: Vendors::EGifter::EGifterService.vendor_name)
      e_gifter_best_seller_items.each do |item|
        FactoryGirl.create(:vendor_item, name: item, vendor: e_gifter)
      end
      FactoryGirl.create_list(:vendor_item, 5, vendor: e_gifter)

      get "/api/v1/best_sellers", headers: headers
      expect(response).to be_successful
      best_seller_items_count = (pulse_best_seller_items + florist_best_seller_items + e_gifter_best_seller_items).count
      expect(parsed_response['data']['best_sellers'].count).to eql(best_seller_items_count)
      expect(parsed_response['data']['best_sellers'].count).not_to eql(VendorItem.count)
    end
  end

  def pulse_best_seller_items
    [
      'Personal Spa and Fitness for One',
      'Golf Tee Time for Two',
      'Dinner for Two',
      'NFL Football Single Night Package',
      'SeaWorld and San Diego Zoo Family Experience',
    ]
  end

  def e_gifter_best_seller_items
    [
      'BBBEYOND',
      'THD',
      'ITUNESC',
      'UBER',
      'PANERA',
    ]
  end

  def florist_best_seller_items
    [
      'The Bright Autumn Centerpiece',
      'Sunny Sunflowers',
      'The Blooming Masterpiece Rose Bouquet',
      'Steal the Show',
      'A Little Pink Me Up',
    ]
  end
end
