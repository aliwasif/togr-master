require 'rails_helper'
require 'spec_helper'

describe V1::Endpoints::Vendors::ExperiencePackages do
  include GrapeRouteHelpers::NamedRouteMatcher
  let(:user) { FactoryGirl.create(:user) }
  let(:token) { FakeDoorkeeperToken.new(user) }
  let(:vendor) { FactoryGirl.create(:vendor, name: Vendors::PulseExperiential::PulseExperientialService.vendor_name) }
  let(:vendor_country) { FactoryGirl.create(:vendor_country, vendor: vendor) }
  let(:vendor_state) { FactoryGirl.create(:vendor_state, country: vendor_country) }
  let(:any_city) { FactoryGirl.create(:vendor_city, name: VendorCity::ANY_CITY, country: vendor_country, state: vendor_state) }
  let(:vendor_city) do
    any_city
    FactoryGirl.create(:vendor_city, country: vendor_country, state: vendor_state)
  end
  let(:headers) do
    {
      'Authorization':  "Bearer faketoken",
      'Content-Type': 'application/json',
    }
  end

  before(:each) do
    allow_any_instance_of(Doorkeeper::Grape::Helpers).to receive(:doorkeeper_token) { token }
  end

  context 'GET /api/v1/cities/:city_id/experience_packages' do
    before(:each) do
      categories = FactoryGirl.create_list(:vendor_category, 5, vendor: vendor)
      vendor_items = FactoryGirl.create_list(:vendor_item, 25, vendor: vendor)
      vendor_items.each do |vendor_item|
        vendor_item.categories = categories.sample(rand(1..5))
        vendor_item.cities = [vendor_city]
      end
    end

    it 'functions properly' do
      get "/api/v1/cities/#{vendor_city.id}/experience_packages", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['categories'].count).to eql(vendor.categories.count)
      parsed_response['data']['categories'].each do |category_object|
        category = vendor.categories.find(category_object['id'])
        experience_packages_ids = category.items.pluck(:id)
        experience_packages_ids_count = experience_packages_ids.count
        expect(experience_packages_ids_count).to eql(category_object['experience_packages_count'])

        category_object['experience_packages'].each do |experience_package_object|
          expect(experience_packages_ids).to include(experience_package_object['id'])
        end

        expect(category_object['experience_packages'].count).to be <= 5
      end
    end
  end

  context 'GET /api/v1/cities/:city_id/experience_packages?query=package name' do
    let(:vendor_items) { FactoryGirl.create_list(:vendor_item, 15, vendor: vendor) }

    before(:each) do
      vendor_items.each do |vendor_item|
        vendor_item.cities = [vendor_city]
      end
    end

    it 'functions properly' do
      names = ['carvajal', 'carvalho', 'car', 'dacar', 'cicaro']
      names.each do |name|
        item = FactoryGirl.create(:vendor_item, vendor: vendor, name: name)
        item.cities = [vendor_city]
        FactoryGirl.create(:vendor_item, vendor: vendor, name: "#{name} 2")
      end

      get "/api/v1/cities/#{vendor_city.id}/experience_packages?query=car", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['experience_packages'].count).to be <= 5
    end
  end

  context 'GET /api/v1/categories/:category_id/cities/:city_id/experience_packages' do
    let(:category) { FactoryGirl.create(:vendor_category, vendor: vendor) }
    let(:vendor_items) { FactoryGirl.create_list(:vendor_item, 15, vendor: vendor) }

    before(:each) do
      vendor_items.each do |vendor_item|
        vendor_item.categories = [category]
        vendor_item.cities = [vendor_city]
      end
    end

    it 'functions properly' do
      get "/api/v1/categories/#{category.id}/cities/#{vendor_city.id}/experience_packages",
          headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '1'
      expect(parsed_response['data']['experience_packages'].count).to be <= 5

      get "/api/v1/categories/#{category.id}/cities/#{vendor_city.id}/experience_packages?page=2",
          headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '2'
      expect(parsed_response['data']['experience_packages'].count).to be <= 5

      get "/api/v1/categories/#{category.id}/cities/#{vendor_city.id}/experience_packages?page=3",
          headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '3'
      expect(parsed_response['data']['experience_packages'].count).to be <= 5

      get "/api/v1/categories/#{category.id}/cities/#{vendor_city.id}/experience_packages?page=4",
          headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '4'
      expect(parsed_response['data']['experience_packages'].count).to be_zero
    end
  end
end
