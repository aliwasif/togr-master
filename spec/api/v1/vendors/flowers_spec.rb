require 'rails_helper'
require 'spec_helper'

describe V1::Endpoints::Vendors::Flowers do
  include GrapeRouteHelpers::NamedRouteMatcher
  let(:user) { FactoryGirl.create(:user) }
  let(:token) { FakeDoorkeeperToken.new(user) }
  let(:vendor) { FactoryGirl.create(:vendor, name: Vendors::FloristOne::FloristOneService.vendor_name) }
  let(:headers) do
    {
      'Authorization':  "Bearer faketoken",
      'Content-Type': 'application/json',
    }
  end

  before(:each) do
    allow_any_instance_of(Doorkeeper::Grape::Helpers).to receive(:doorkeeper_token) { token }
  end

  context 'GET /api/v1/flowers' do
    before(:each) do
      names = ['carvajal', 'carvalho', 'car', 'dacar', 'cicaro']
      names.each do |name|
        FactoryGirl.create(:flower, vendor: vendor, name: name)
      end
      FactoryGirl.create_list(:flower, 5, vendor: vendor)
    end

    it 'responds with flowers which match search term' do
      get "/api/v1/flowers?query=car", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['flowers'].count).to eql(5)
      expect(vendor.items.count).to eql(10)
      response_flower_ids = []
      parsed_response['data']['flowers'].each do |flower_object|
        expect(flower_object['name']).to include('car')
        response_flower_ids << flower_object['id']
      end
      vendor.items.where.not(id: response_flower_ids).each do |flower|
        expect(flower.name).not_to include('car')
      end
    end
  end

  context 'GET /api/v1/flowers/by_occasion' do
    before(:each) do
      categories = FactoryGirl.create_list(:vendor_category, 5, vendor: vendor)
      vendor_items = FactoryGirl.create_list(:flower, 25, vendor: vendor)
      vendor_items.each do |vendor_item|
        vendor_item.categories = categories.sample(rand(1..3))
      end
    end

    it 'responds with the list of flowers grouped by occasion' do
      get '/api/v1/flowers/by_occasion', headers: headers
      expect(response).to be_successful

      expect(parsed_response['data']['occasions'].count).to eql(vendor.categories.count)
      parsed_response['data']['occasions'].each do |occasion_object|
        category = vendor.categories.find(occasion_object['id'])
        category_flower_ids = category.items.pluck(:id)
        category_flowers_count = category_flower_ids.count
        expect(category_flowers_count).to eql(occasion_object['flowers_count'])

        occasion_object['flowers'].each do |flower|
          expect(category_flower_ids).to include(flower['id'])
        end

        expect(occasion_object['flowers'].count).to be <= 5
      end
    end
  end

  context 'GET /api/v1/flowers/by_price_range' do
    before(:each) do
      FloristOne::PRICE_RANGES.each do |price_range|
        7.times do
          min_price = FloristOne.min_price(price_range).to_f
          max_price = FloristOne.max_price(price_range).to_f
          FactoryGirl.create(:flower, vendor: vendor, price: rand(min_price..max_price))
        end
      end
    end

    it 'responds with a list of flowers grouped by price_range' do
      get '/api/v1/flowers/by_price_range', headers: headers
      expect(response).to be_successful

      expect(parsed_response['data']['price_ranges'].count).to eql(FloristOne::PRICE_RANGES.count)
      parsed_response['data']['price_ranges'].each do |price_range|
        min_price = FloristOne.min_price(price_range['value']).to_f
        max_price = FloristOne.max_price(price_range['value']).to_f
        price_range['flowers'].each do |flower|
          expect(flower['price']).to be <= max_price
          expect(flower['price']).to be >= min_price
        end
        flowers_count_from_db = vendor.items.where(price: min_price..max_price).count
        expect(price_range['flowers_count']).to eql(flowers_count_from_db)
        expect(price_range['flowers'].count).to be <= 5
      end
    end
  end

  context 'GET /api/v1/price_ranges/:price_range/flowers' do
    let(:price_range) { FloristOne::PRICE_RANGES.first }
    let(:min_price) { FloristOne.min_price(price_range).to_f }
    let(:max_price) { FloristOne.max_price(price_range).to_f }

    before(:each) do
      10.times { FactoryGirl.create(:flower, vendor: vendor, price: rand(min_price..max_price)) }
    end

    it 'responds with flowers of a single price_range with pagination' do
      get "/api/v1/price_ranges/#{price_range}/flowers?page=1", headers: headers
      expect(response).to be_successful
      flower_ids = vendor.items.where(price: min_price..max_price).pluck(:id)
      page1_flowers = parsed_response['data']['flowers']
      page1_flowers.each do |flower|
        expect(flower_ids).to include(flower['id'])
      end
      expect(parsed_response['data']['page']).to eql '1'
      expect(parsed_response['data']['flowers'].count).to be <= 5

      get "/api/v1/price_ranges/#{price_range}/flowers?page=2", headers: headers
      expect(response).to be_successful
      page2_flowers = parsed_response['data']['flowers']
      expect(page2_flowers).not_to eql(page1_flowers)
      page2_flowers.each do |flower|
        expect(flower_ids).to include(flower['id'])
      end
      expect(parsed_response['data']['page']).to eql '2'
      expect(parsed_response['data']['flowers'].count).to be <= 5

      get "/api/v1/price_ranges/#{price_range}/flowers?page=3", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '3'
      expect(parsed_response['data']['flowers'].count).to be_zero
    end
  end

  context 'GET /api/v1/occasions/:occasion_id/flowers' do
    let(:occasion) { FactoryGirl.create(:vendor_category, vendor: vendor) }

    before(:each) do
      flowers = FactoryGirl.create_list(:flower, 10, vendor: vendor)
      flowers.each { |flower| flower.categories = [occasion] }
    end

    it 'responds with flowers of a single occasion with pagination' do
      get "/api/v1/occasions/#{occasion.id}/flowers?page=1", headers: headers
      expect(response).to be_successful
      category_flower_ids = occasion.items.pluck(:id)
      page1_flowers = parsed_response['data']['flowers']
      page1_flowers.each do |flower|
        expect(category_flower_ids).to include(flower['id'])
      end
      expect(parsed_response['data']['page']).to eql '1'
      expect(parsed_response['data']['flowers'].count).to be <= 5

      get "/api/v1/occasions/#{occasion.id}/flowers?page=2", headers: headers
      expect(response).to be_successful
      page2_flowers = parsed_response['data']['flowers']
      expect(page2_flowers).not_to eql(page1_flowers)
      page2_flowers.each do |flower|
        expect(category_flower_ids).to include(flower['id'])
      end
      expect(parsed_response['data']['page']).to eql '2'
      expect(parsed_response['data']['flowers'].count).to be <= 5

      get "/api/v1/occasions/#{occasion.id}/flowers?page=3", headers: headers
      expect(response).to be_successful
      expect(parsed_response['data']['page']).to eql '3'
      expect(parsed_response['data']['flowers'].count).to be_zero
    end
  end
end
