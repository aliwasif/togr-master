require 'rails_helper'
require 'spec_helper'

describe 'Login' do
  let(:user) { FactoryGirl.create(:user, :confirmed) }
  let(:login_params) { { email: user.email, password: 'admin123', grant_type: 'password' } }

  context 'POST /oauth/token' do
    it 'let the users login' do
      expect(user.access_tokens.count).to be_zero
      post '/oauth/token', params: login_params
      expect(response).to be_successful
      expect(user.access_tokens.count).not_to be_zero
      expect(parsed_response['data']['access']['access_token']).to be_present
      expect(user.access_tokens.first.expired?).to be false
    end

    it 'does not let a user with wrong password address login' do
      login_params[:password] = 'wrong_password'
      post '/oauth/token', params: login_params
      expect(response).to have_http_status(:unauthorized)
      expect(user.access_tokens.count).to be_zero
      expect(response_contains_error?(key: 'invalid_grant')).to be true
    end

    it 'does not let a user with wrong email address login' do
      fake_login_params = { email: 'invalid@email.com', password: 'abc', grant_type: 'password' }
      post '/oauth/token', params: fake_login_params
      expect(response).to have_http_status(:unauthorized)
      expect(user.access_tokens.count).to be_zero
      expect(response_contains_error?(key: 'invalid_grant')).to be true
    end

    it 'does not let an unconfirmed user login' do
      unconfirmed_user = FactoryGirl.create(:user)
      unconfirmed_login_params = {
        email: unconfirmed_user.email,
        password: 'admin123',
        grant_type: 'password',
      }
      post '/oauth/token', params: unconfirmed_login_params
      expect(response).to have_http_status(:unauthorized)
      expect(user.access_tokens.count).to be_zero
      expect(response_contains_error?(key: 'invalid_grant')).to be true
    end
  end
end
