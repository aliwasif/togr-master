require 'rails_helper'
require 'spec_helper'

describe V1::Endpoints::GreetingVideos do
  include GrapeRouteHelpers::NamedRouteMatcher

  let(:user) {FactoryGirl.create(:user)}
  let(:user_b) {FactoryGirl.create(:user)}
  let(:application) {FactoryGirl.create(:application)}
  let(:greeting_video) {FactoryGirl.create(:greeting_video, user: user)}

  context 'Positive Cases: ' do

    xit 'Verify that "Add a Video" API returns status (201 - created) upon sending a POST request' do
      request_body = {
          greeting_video: {
              message: "Video Message",
              base_64_video: test_video_base64
          }
      }
      token = oauth_token(application, user)
      post(
          api_v1_users_me_greeting_videos_path,
          params: request_body.to_json,
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:created)
      expect(user.greeting_videos.count).to eq(1)
    end

    xit 'Verify that "Get All Videos" API returns status (200 - ok) upon sending a GET request' do
      token = oauth_token(application, user)
      create_greeting_videos(user)
      get(
          api_v1_users_me_greeting_videos_path,
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:ok)
      videos = parsed_response['data']['greeting_videos']
      validate_videos(videos[0])
    end

    xit 'Verify that "Get a Videos" API returns status (200 - ok) upon sending a GET request' do
      token = oauth_token(application, user)
      create_greeting_videos(user)
      get(
          api_v1_users_me_greeting_videos_path(id: user.id),
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:ok)
      videos = parsed_response['data']['greeting_video']
      validate_videos(videos)
    end

    xit 'Verify that "Remove a Videos" API returns status (204 - no_content) upon sending a DELETE request' do
      token = oauth_token(application, user)
      create_greeting_videos(user)
      delete(
          api_v1_users_me_greeting_videos_remove_path(id: user.id),
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:no_content)
      expect(response).to be_truthy
    end
  end

  context 'Negative Cases: ' do

    it 'Verify that "Add a Video" API returns status (406 - not_acceptable) upon sending a POST request with empty Video path/message' do
      request_body = {
          greeting_video: {
              base_64_video: "",
              message: "",
          }
      }
      token = oauth_token(application, user)
      post(
          api_v1_users_me_greeting_videos_path,
          params: request_body.to_json,
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_acceptable)
      expect(response_contains_error?(key: 'base_64_video')).to be true
      expect(response_contains_error?(key: 'message')).to be true
    end

    it 'Verify that "Add a Video" API returns status (406 - not_acceptable) upon sending a POST request with empty Video path' do
      request_body = {
          "greeting_video": {
              "message": "Video Message",
              "base_64_video": ""
          }
      }
      token = oauth_token(application, user)
      post(
          api_v1_users_me_greeting_videos_path,
          params: request_body.to_json,
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_acceptable)
    end

    it 'Verify that "Add a Video" API returns status (406 - not_acceptable) upon sending a POST request with empty message' do
      request_body = {
          "greeting_video": {
              "message": "",
              "base_64_video": test_video_base64
          }
      }
      token = oauth_token(application, user)
      post(
          api_v1_users_me_greeting_videos_path,
          params: request_body.to_json,
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_acceptable)
    end

    xit 'Verify that "Remove a Videos" API returns status (404 - not_found) upon sending a DELETE request with bogus ID' do
      token = oauth_token(application, user)
      create_greeting_videos(user)
      delete(
          api_v1_users_me_greeting_videos_remove_path(id: 99999),
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_found)
    end

    xit 'Verify that "Remove a Videos" API returns status (401 - unauthorized) upon sending a DELETE request without token' do
      create_greeting_videos(user)
      delete(
          api_v1_users_me_greeting_videos_remove_path(id: user.id),
      )
      expect(response).to have_http_status(:unauthorized)
    end

    xit 'Verify that "Remove a Videos" API returns status (404 - not_found) upon creating video from User-A & removing it from User-B' do
      create_greeting_videos(user)
      token = oauth_token(application, user_b)
      delete(
          api_v1_users_me_greeting_videos_remove_path(id: user.id),
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_found)
    end

    xit 'Verify that "Get a Videos" API returns status (404 - not_found) upon sending a GET request with bogus ID' do
      create_greeting_videos(user)
      token = oauth_token(application, user)
      get(
          api_v1_users_me_greeting_videos_path(id: 9999),
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_found)
    end

    xit 'Verify that "Get a Videos" API returns status (401 - unauthorized) upon sending a GET request without token' do
      create_greeting_videos(user)
      get(
          api_v1_users_me_greeting_videos_path(id: user.id),
      )
      expect(response).to have_http_status(:unauthorized)
    end

    xit 'Verify that "Get a Videos" API returns status (404 - not_found) upon creating video from User-A & getting it from User-B' do
      create_greeting_videos(user)
      token = oauth_token(application, user_b)
      get(
          api_v1_users_me_greeting_videos_path(id: user.id),
          headers: authorization_header(token)
      )
      expect(response).to have_http_status(:not_found)
    end
  end

  def create_greeting_videos(user)
    FactoryGirl.create(:greeting_video, :with_video, user: user)
  end

  def validate_videos(video)
    expect(video['message']).to be_present
    expect(video['video']).to be_present
  end
end
