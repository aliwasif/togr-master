require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TogrApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.paths.add File.join('app', 'api'), glob: File.join('**', '*.rb')
    config.autoload_paths += Dir[Rails.root.join('app', 'api', '*')]
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: [:get, :post, :options]
      end
    end
    config.active_job.queue_name_prefix = Rails.env
    config.autoload_paths << "#{Rails.root}/lib"
  end
end

CarrierWave.configure do |config|
  config.storage    = :aws
  config.aws_bucket = 'togr' # for AWS-side bucket access permissions config, see section below
  # config.aws_acl    = 'public-read'

  # The maximum period for authenticated_urls is only 7 days.
  config.aws_authenticated_url_expiration = 60 * 60 * 24 * 7

  # Set custom options such as cache control to leverage browser caching
  config.aws_attributes = {
    expires: 1.week.from_now.httpdate,
    cache_control: 'max-age=604800'
  }

  config.aws_credentials = {
    access_key_id:     'AKIAJNHRBMXHNNSER52A',
    secret_access_key: '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',
    region:            'us-west-2', # Required
    stub_responses:    Rails.env.test? # Optional, avoid hitting S3 actual during tests
  }
end

