module Devise
  def self.friendly_token(length = 6)
    # To calculate real characters, we must perform this operation.
    # See SecureRandom.urlsafe_base64
    # rlength = (length * 3) / 4
    # SecureRandom.urlsafe_base64(rlength).tr('lIO0', 'sxyz')
    rand(10000..100000).to_s
  end
end
