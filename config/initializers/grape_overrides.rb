Grape::Exceptions::ValidationErrors.class_eval do
  def as_json(**_opts)
    errors.map do |k, v|
      {
        key: k.first&.gsub(/(?<=\[).+?(?=\])/).first,
        message: v.first,
      }
    end
  end
end