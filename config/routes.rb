require 'sidekiq/web'
Rails.application.routes.draw do
  get 'authorize_net', to: "authorize_net#index"
  use_doorkeeper
  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  mount API => '/'
  ActiveAdmin.routes(self)
  # root to: 'admin#index'
  authenticate :user do
    mount Sidekiq::Web => '/sidekiq'
  end
end
