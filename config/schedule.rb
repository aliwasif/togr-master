set :output, File.join(Whenever.path, "log", "cron.log")

every 1.week, at: '4:30 am' do
  rake "notifications:truncate"
end
