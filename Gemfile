source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.0'
# Use Postgres as the database for Active Record
gem "pg", '1.0.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

gem 'rubocop-airbnb', '1.5.0'

gem 'will_paginate'
gem 'stripe'
gem 'progress_bar'
gem 'whenever', require: false
gem "rolify"
gem 'sidekiq', '5.2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false


gem 'devise'
gem 'grape', '1.1.0'
gem 'grape-swagger'
gem 'grape-swagger-ui'
gem 'activeadmin'
gem 'twilio-ruby', '~> 5.10.5'
gem 'carrierwave-aws'
gem 'carrierwave-base64'
gem 'ancestry'
gem 'activeadmin_json_editor', '~> 0.0.7'

gem 'dotenv-rails', :groups => [:development, :test]
gem 'doorkeeper'
gem 'doorkeeper-grants_assertion'
gem 'rack-cors', require: 'rack/cors'
gem 'sendgrid'
gem "aws-sdk-s3", require: false
gem 'mini_magick'
gem 'fcm'
gem 'grape-entity'
gem 'public_activity'
gem 'geokit-rails'
gem 'firebase'
gem 'geocoder'
gem 'sendgrid-ruby'
gem 'sidekiq-scheduler'
gem 'rack', '2.0.8'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'database_cleaner'
  gem 'factory_bot'
  gem 'rspec-rails', '~> 3.5.2'
  gem 'grape-route-helpers'
  gem 'faker'
  gem 'pry'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'debase'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
