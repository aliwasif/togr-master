class AddWorkerToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :worker_id, :integer
  end
end
