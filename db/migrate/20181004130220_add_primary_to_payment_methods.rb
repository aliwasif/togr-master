class AddPrimaryToPaymentMethods < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_methods, :primary, :boolean
  end
end
