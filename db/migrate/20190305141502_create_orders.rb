class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.references :user
      t.references :payment_method
      t.references :session
      t.float :total
      t.float :tip
      t.float :sub_total
      t.string :status

      t.timestamps
    end
  end
end
