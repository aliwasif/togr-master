class AddRecommendedToItems < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :recommended, :boolean, default: false
  end
end
