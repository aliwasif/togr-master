class AddCapturedToSessionRequests < ActiveRecord::Migration[5.2]
  def change
    add_column :session_requests, :captured, :boolean
  end
end
