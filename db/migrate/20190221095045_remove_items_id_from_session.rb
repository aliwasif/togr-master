class RemoveItemsIdFromSession < ActiveRecord::Migration[5.2]
  def change
    remove_column :sessions, :items_id
  end
end
