class AddJobNumberToJobs < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :job_number, :string
  end
end
