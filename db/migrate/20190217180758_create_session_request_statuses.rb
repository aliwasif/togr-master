class CreateSessionRequestStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :request_statuses do |t|
      t.string :message
      t.integer :status
      t.references :statusable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
