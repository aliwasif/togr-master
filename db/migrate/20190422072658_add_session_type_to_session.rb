class AddSessionTypeToSession < ActiveRecord::Migration[5.2]
  def change
    add_column :sessions, :session_type, :string
  end
end
