class CreateBusinessProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :business_profiles do |t|
      t.string :title
      t.integer :service_id
      t.references :user
      t.json :tags, default: []

      t.timestamps
    end
  end
end
