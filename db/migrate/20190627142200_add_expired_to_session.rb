class AddExpiredToSession < ActiveRecord::Migration[5.2]
  def change
    add_column :sessions, :expired, :boolean
  end
end
