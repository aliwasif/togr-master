class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :title_english
      t.string :title_arabic
      t.string :description_english
      t.string :description_arabic
      t.json :tags, default: []
      t.timestamps
    end
  end
end
