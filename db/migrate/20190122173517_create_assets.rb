class CreateAssets < ActiveRecord::Migration[5.2]
  def change
    create_table :assets do |t|
      t.string :name
      t.string :image
      t.references :imageable, polymorphic: true, index: true

      t.timestamps
    end

    add_index :assets, [:imageable_id, :name], unique: true
  end
end
