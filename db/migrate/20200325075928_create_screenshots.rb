class CreateScreenshots < ActiveRecord::Migration[5.2]
  def change
    create_table :screenshots do |t|
      t.references :item
      t.references :session
      t.references :user
      t.timestamps
    end
  end
end
