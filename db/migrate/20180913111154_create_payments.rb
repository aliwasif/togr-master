class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.references :chargeable, polymorphic: true, index: true
      t.string :charge_type
      t.string :charge_id
      t.string :status
      t.float :amount
      t.timestamps
    end
  end
end
