class AddExtraPhotosPriceAndExtraPhotosCommissionToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :packages, :extra_photos_commission, :float
  end
end
