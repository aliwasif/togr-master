class AddStatusToBusinessProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :business_profiles, :status, :string
  end
end
