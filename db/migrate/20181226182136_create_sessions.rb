class CreateSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :sessions do |t|
      t.references :worker, index: true
      t.references :items
      t.references :requestable, polymorphic: true, index: true

      t.timestamps
    end

    add_foreign_key :sessions, :users, column: :worker_id, primary_key: :id
  end
end
