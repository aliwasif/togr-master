class AddPhoneNumbersToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :phone_numbers, :json, default: []
  end
end
