class AddSessionRequestToPayments < ActiveRecord::Migration[5.2]
  def change
    add_column :payments, :session_request_id, :integer
  end
end
