class AddDescriptionToBusinessProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :business_profiles, :description, :string
  end
end
