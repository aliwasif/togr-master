class AddWorkerArrivedAtLocationToSessionRequests < ActiveRecord::Migration[5.2]
  def change
    add_column :session_requests, :worker_arrived_at_location, :json, default: {}
  end
end
