class AddAdvanceToSessionRequests < ActiveRecord::Migration[5.2]
  def change
    add_column :session_requests, :advance, :integer
  end
end
