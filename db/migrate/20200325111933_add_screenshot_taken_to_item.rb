class AddScreenshotTakenToItem < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :screenshot, :boolean
  end
end
