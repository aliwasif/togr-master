class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :s3_id
      t.references :session
      t.timestamps
    end
  end
end
