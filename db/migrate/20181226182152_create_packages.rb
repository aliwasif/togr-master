class CreatePackages < ActiveRecord::Migration[5.2]
  def change
    create_table :packages do |t|
        t.string :package_type
        t.string :photo
        t.integer :amount
        t.integer :worker_remuneration
        t.string :expiry
        t.integer :session_length
        t.string :photos_taken
        t.integer :photos_count
        t.float :extra_photos_price
        t.text :info

        t.timestamps
    end
  end
end
