class AddExpiryDateToSessions < ActiveRecord::Migration[5.2]
  def change
    add_column :sessions, :expiry_date, :datetime
  end
end
