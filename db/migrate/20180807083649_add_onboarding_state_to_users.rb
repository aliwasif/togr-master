class AddOnboardingStateToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :onboarding_state, :integer, default: 0
  end
end
