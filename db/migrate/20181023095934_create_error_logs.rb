class CreateErrorLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :error_logs do |t|
      t.belongs_to :origin, polymorphic: true
      t.json :error
      t.timestamps
    end
  end
end
