class AddPackageIdToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :package_id, :integer
  end
end
