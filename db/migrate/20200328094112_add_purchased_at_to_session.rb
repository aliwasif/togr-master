class AddPurchasedAtToSession < ActiveRecord::Migration[5.2]
  def change
    add_column :sessions, :purchased_at, :datetime
  end
end
