class CreateAppSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :app_settings do |t|
      t.string :name
      t.string :value
      t.string :setting_type
      t.string :description
      t.json :settings, default: {}
      t.timestamps
    end
  end
end
