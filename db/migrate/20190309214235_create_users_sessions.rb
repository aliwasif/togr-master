class CreateUsersSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :users_sessions do |t|
      t.references :user
      t.references :session
      t.references :session_request
      t.references :worker, index: true

      t.timestamps
    end

    add_foreign_key :users_sessions, :users, column: :worker_id, primary_key: :id
  end
end
