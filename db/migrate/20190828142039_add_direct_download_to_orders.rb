class AddDirectDownloadToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :direct_download, :boolean
  end
end
