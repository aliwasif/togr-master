class AddS3IdToAssets < ActiveRecord::Migration[5.2]
  def change
    add_column :assets, :s3_id, :string
  end
end
