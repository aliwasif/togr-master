class AddDeclinedUsersToSessionRequests < ActiveRecord::Migration[5.2]
  def change
    add_column :session_requests, :declined_users, :json, default: []
  end
end
