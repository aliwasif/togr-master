class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.datetime :estimated_time
      t.integer :payment_method
      t.integer :cost
      t.text :detail
      t.references :user, index: true
      t.references :worker, index: true
      t.references :business_profile
      t.datetime :started_at
      t.datetime :completed_at
      t.timestamps
      t.timestamps
    end
    add_foreign_key :jobs, :users, column: :worker_id, primary_key: :id
  end
end
