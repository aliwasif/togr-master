class CreatePaymentMethods < ActiveRecord::Migration[5.2]
  def change
    enable_extension "hstore"
    create_table :payment_methods do |t|
      t.references :user, foreign_key: true
      t.hstore :json

      t.timestamps
    end
  end
end
