class AddPaymentMethodToSessionRequests < ActiveRecord::Migration[5.2]
  def change
    add_column :session_requests, :payment_method_id, :integer
  end
end
