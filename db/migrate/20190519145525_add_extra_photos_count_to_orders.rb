class AddExtraPhotosCountToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :extra_photos_count, :integer
  end
end
