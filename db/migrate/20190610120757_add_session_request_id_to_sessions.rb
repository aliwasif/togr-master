class AddSessionRequestIdToSessions < ActiveRecord::Migration[5.2]
  def change
    add_column :sessions, :session_request_id, :integer
  end
end
