class AddCapturedToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :captured, :boolean
  end
end
