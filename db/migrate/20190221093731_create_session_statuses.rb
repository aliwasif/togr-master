class CreateSessionStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :session_statuses do |t|
      t.string :name
      t.references :statusable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
