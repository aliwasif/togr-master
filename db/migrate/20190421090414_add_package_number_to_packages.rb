class AddPackageNumberToPackages < ActiveRecord::Migration[5.2]
  def change
    add_column :packages, :package_number, :integer
  end
end
