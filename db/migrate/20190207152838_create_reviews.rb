class CreateReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.integer :rating
      t.integer :medal
      t.boolean :allow_post_to_social_media
      t.boolean :allow_post_to_portfolio
      t.text :comment
      t.references :session
      t.references :user, index: true
      t.references :worker, index: true

      t.timestamps
    end
    add_foreign_key :reviews, :users, column: :worker_id, primary_key: :id
    add_index :reviews, [:user_id, :worker_id, :session_id], unique: true
  end
end
