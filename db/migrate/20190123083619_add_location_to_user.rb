class AddLocationToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :location, :json, default: {}
  end
end
