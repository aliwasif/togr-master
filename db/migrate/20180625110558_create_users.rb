class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.integer :age
      t.date :dob
      t.integer :gender
      t.string :phone_number
      t.string :image
      t.string :provider
      t.string :uid

      t.timestamps
    end
  end
end
