class AddWorkerUidToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :worker_uid, :string
  end
end
