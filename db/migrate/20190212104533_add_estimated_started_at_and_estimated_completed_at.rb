class AddEstimatedStartedAtAndEstimatedCompletedAt < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :estimated_started_at, :datetime
    add_column :jobs, :estimated_completed_at, :datetime
  end
end
