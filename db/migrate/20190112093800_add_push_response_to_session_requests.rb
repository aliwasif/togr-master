class AddPushResponseToSessionRequests < ActiveRecord::Migration[5.2]
  def change
    add_column :session_requests, :push_response, :json, default: {}
  end
end
