class AddCompletedAtToSessions < ActiveRecord::Migration[5.2]
  def change
    add_column :sessions, :completed_at, :datetime
  end
end
