class CreateSessionRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :session_requests do |t|
      t.references :user
      t.references :worker
      t.references :package
      t.json :user_location, default: {}
      t.json :worker_location, default: {}
      t.string :status

      t.timestamps
    end

    add_foreign_key :session_requests, :users, column: :worker_id, primary_key: :id
  end
end
