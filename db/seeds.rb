def perform
  if Rails.env.development?
    puts 'seeding data for development environment...'
    unless AdminUser.where(email: 'admin@example.com').exists?
      puts 'creating admin user...'
      AdminUser.create!(email: 'admin@togr.com', password: 'admin@12',
                        password_confirmation: 'admin@12')
    end

    Doorkeeper::Application.all.each do |app|
      app.confidential = true
      app.save
    end
  end
end

perform
