# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_28_094112) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "hstore"
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "activities", force: :cascade do |t|
    t.string "trackable_type"
    t.bigint "trackable_id"
    t.string "owner_type"
    t.bigint "owner_id"
    t.string "key"
    t.text "parameters"
    t.string "recipient_type"
    t.bigint "recipient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type"
    t.index ["owner_type", "owner_id"], name: "index_activities_on_owner_type_and_owner_id"
    t.index ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type"
    t.index ["recipient_type", "recipient_id"], name: "index_activities_on_recipient_type_and_recipient_id"
    t.index ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type"
    t.index ["trackable_type", "trackable_id"], name: "index_activities_on_trackable_type_and_trackable_id"
  end

  create_table "addresses", force: :cascade do |t|
    t.string "street"
    t.string "city"
    t.string "state"
    t.string "zip"
    t.string "country"
    t.boolean "primary"
    t.string "addressable_type"
    t.bigint "addressable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "app_settings", force: :cascade do |t|
    t.string "name"
    t.string "value"
    t.string "setting_type"
    t.string "description"
    t.json "settings", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "assets", force: :cascade do |t|
    t.string "name"
    t.string "image"
    t.string "imageable_type"
    t.bigint "imageable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "s3_id"
    t.index ["imageable_id", "name"], name: "index_assets_on_imageable_id_and_name", unique: true
    t.index ["imageable_type", "imageable_id"], name: "index_assets_on_imageable_type_and_imageable_id"
  end

  create_table "business_profiles", force: :cascade do |t|
    t.string "title"
    t.integer "service_id"
    t.bigint "user_id"
    t.json "tags", default: []
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.string "status"
    t.index ["user_id"], name: "index_business_profiles_on_user_id"
  end

  create_table "devices", force: :cascade do |t|
    t.boolean "enabled"
    t.string "token"
    t.integer "user_id"
    t.string "platform"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "error_logs", force: :cascade do |t|
    t.string "origin_type"
    t.bigint "origin_id"
    t.json "error"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["origin_type", "origin_id"], name: "index_error_logs_on_origin_type_and_origin_id"
  end

  create_table "items", force: :cascade do |t|
    t.string "s3_id"
    t.bigint "session_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.boolean "recommended", default: false
    t.boolean "screenshot"
    t.index ["session_id"], name: "index_items_on_session_id"
  end

  create_table "jobs", force: :cascade do |t|
    t.datetime "estimated_time"
    t.integer "payment_method"
    t.integer "cost"
    t.text "detail"
    t.bigint "user_id"
    t.bigint "worker_id"
    t.bigint "business_profile_id"
    t.datetime "started_at"
    t.datetime "completed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.string "job_number"
    t.datetime "estimated_started_at"
    t.datetime "estimated_completed_at"
    t.index ["business_profile_id"], name: "index_jobs_on_business_profile_id"
    t.index ["user_id"], name: "index_jobs_on_user_id"
    t.index ["worker_id"], name: "index_jobs_on_worker_id"
  end

  create_table "locations", force: :cascade do |t|
    t.integer "distance"
    t.string "locatable_type"
    t.bigint "locatable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "lat", precision: 10, scale: 6
    t.decimal "lng", precision: 10, scale: 6
    t.string "address"
    t.index ["locatable_type", "locatable_id"], name: "index_locations_on_locatable_type_and_locatable_id"
  end

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer "resource_owner_id", null: false
    t.bigint "application_id", null: false
    t.string "token", null: false
    t.integer "expires_in", null: false
    t.text "redirect_uri", null: false
    t.datetime "created_at", null: false
    t.datetime "revoked_at"
    t.string "scopes"
    t.index ["application_id"], name: "index_oauth_access_grants_on_application_id"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer "resource_owner_id"
    t.bigint "application_id"
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at", null: false
    t.string "scopes"
    t.string "previous_refresh_token", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_tokens_on_application_id"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string "name", null: false
    t.string "uid", null: false
    t.string "secret", null: false
    t.text "redirect_uri", null: false
    t.string "scopes", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true
  end

  create_table "order_items", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_order_items_on_item_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "payment_method_id"
    t.bigint "session_id"
    t.float "total"
    t.float "tip"
    t.float "sub_total"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "worker_id"
    t.integer "package_id"
    t.integer "extra_photos_count"
    t.boolean "captured"
    t.boolean "direct_download"
    t.index ["payment_method_id"], name: "index_orders_on_payment_method_id"
    t.index ["session_id"], name: "index_orders_on_session_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "packages", force: :cascade do |t|
    t.string "package_type"
    t.string "photo"
    t.integer "amount"
    t.integer "worker_remuneration"
    t.string "expiry"
    t.integer "session_length"
    t.string "photos_taken"
    t.integer "photos_count"
    t.float "extra_photos_price"
    t.text "info"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "package_number"
    t.float "extra_photos_commission"
    t.string "details"
  end

  create_table "payment_methods", force: :cascade do |t|
    t.bigint "user_id"
    t.hstore "json"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "primary"
    t.index ["user_id"], name: "index_payment_methods_on_user_id"
  end

  create_table "payments", force: :cascade do |t|
    t.string "chargeable_type"
    t.bigint "chargeable_id"
    t.string "charge_type"
    t.string "charge_id"
    t.string "status"
    t.float "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "session_request_id"
    t.integer "user_id"
    t.index ["chargeable_type", "chargeable_id"], name: "index_payments_on_chargeable_type_and_chargeable_id"
  end

  create_table "request_statuses", force: :cascade do |t|
    t.string "message"
    t.integer "status"
    t.string "statusable_type"
    t.bigint "statusable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["statusable_type", "statusable_id"], name: "index_request_statuses_on_statusable_type_and_statusable_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.integer "rating"
    t.integer "medal"
    t.boolean "allow_post_to_social_media"
    t.boolean "allow_post_to_portfolio"
    t.text "comment"
    t.bigint "session_id"
    t.bigint "user_id"
    t.bigint "worker_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order_id"
    t.index ["session_id"], name: "index_reviews_on_session_id"
    t.index ["user_id", "worker_id", "session_id"], name: "index_reviews_on_user_id_and_worker_id_and_session_id", unique: true
    t.index ["user_id"], name: "index_reviews_on_user_id"
    t.index ["worker_id"], name: "index_reviews_on_worker_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "screenshots", force: :cascade do |t|
    t.bigint "item_id"
    t.bigint "session_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_screenshots_on_item_id"
    t.index ["session_id"], name: "index_screenshots_on_session_id"
    t.index ["user_id"], name: "index_screenshots_on_user_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "title_english"
    t.string "title_arabic"
    t.string "description_english"
    t.string "description_arabic"
    t.json "tags", default: []
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ancestry"
    t.index ["ancestry"], name: "index_services_on_ancestry"
  end

  create_table "session_requests", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "worker_id"
    t.bigint "package_id"
    t.json "user_location", default: {}
    t.json "worker_location", default: {}
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "worker_arrived_at_location", default: {}
    t.json "push_response", default: {}
    t.integer "payment_method_id"
    t.json "declined_users", default: []
    t.integer "advance"
    t.boolean "captured"
    t.index ["package_id"], name: "index_session_requests_on_package_id"
    t.index ["user_id"], name: "index_session_requests_on_user_id"
    t.index ["worker_id"], name: "index_session_requests_on_worker_id"
  end

  create_table "session_statuses", force: :cascade do |t|
    t.string "name"
    t.string "statusable_type"
    t.bigint "statusable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["statusable_type", "statusable_id"], name: "index_session_statuses_on_statusable_type_and_statusable_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.bigint "worker_id"
    t.string "requestable_type"
    t.bigint "requestable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "completed_at"
    t.datetime "expiry_date"
    t.string "status"
    t.string "reason"
    t.string "session_type"
    t.integer "user_id"
    t.integer "session_request_id"
    t.boolean "expired"
    t.boolean "screenshot", default: false
    t.datetime "purchased_at"
    t.index ["requestable_type", "requestable_id"], name: "index_sessions_on_requestable_type_and_requestable_id"
    t.index ["worker_id"], name: "index_sessions_on_worker_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.integer "age"
    t.date "dob"
    t.integer "gender"
    t.string "phone_number"
    t.string "image"
    t.string "provider"
    t.string "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.integer "onboarding_state", default: 0
    t.string "stripe_customer_id"
    t.jsonb "privacy_settings", default: {}, null: false
    t.json "profile", default: {}
    t.json "phone_numbers", default: []
    t.integer "business_type"
    t.string "business_name"
    t.string "worker_uid"
    t.boolean "available"
    t.integer "status", default: 0
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  create_table "users_sessions", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "session_id"
    t.bigint "session_request_id"
    t.bigint "worker_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_users_sessions_on_session_id"
    t.index ["session_request_id"], name: "index_users_sessions_on_session_request_id"
    t.index ["user_id"], name: "index_users_sessions_on_user_id"
    t.index ["worker_id"], name: "index_users_sessions_on_worker_id"
  end

  add_foreign_key "jobs", "users", column: "worker_id"
  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
  add_foreign_key "payment_methods", "users"
  add_foreign_key "reviews", "users", column: "worker_id"
  add_foreign_key "session_requests", "users", on_delete: :cascade
  add_foreign_key "sessions", "users", column: "worker_id"
  add_foreign_key "users_sessions", "users", column: "worker_id"
end
