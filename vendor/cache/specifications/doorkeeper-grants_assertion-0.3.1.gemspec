# -*- encoding: utf-8 -*-
# stub: doorkeeper-grants_assertion 0.3.1 ruby lib

Gem::Specification.new do |s|
  s.name = "doorkeeper-grants_assertion".freeze
  s.version = "0.3.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Tute Costa".freeze, "Nikita Bulai".freeze]
  s.date = "2021-04-07"
  s.description = "Assertion grant extension for Doorkeeper.".freeze
  s.email = ["tutecosta@gmail.com".freeze, "bulajnikita@gmail.com".freeze]
  s.homepage = "https://github.com/doorkeeper-gem/doorkeeper-grants-assertion".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "3.2.16".freeze
  s.summary = "Assertion grant extension for Doorkeeper.".freeze

  s.installed_by_version = "3.2.16" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<railties>.freeze, [">= 5.0"])
    s.add_runtime_dependency(%q<doorkeeper>.freeze, [">= 4.0"])
    s.add_development_dependency(%q<rspec-rails>.freeze, [">= 2.11.4"])
    s.add_development_dependency(%q<capybara>.freeze, ["~> 2.18.0"])
    s.add_development_dependency(%q<factory_bot>.freeze, ["~> 4.8.2"])
    s.add_development_dependency(%q<generator_spec>.freeze, ["~> 0.9.4"])
    s.add_development_dependency(%q<database_cleaner>.freeze, ["~> 1.6.2"])
    s.add_development_dependency(%q<pry>.freeze, [">= 0.11.3"])
    s.add_development_dependency(%q<appraisal>.freeze, ["~> 2.2.0"])
    s.add_development_dependency(%q<omniauth-oauth2>.freeze, ["~> 1.5.0"])
    s.add_development_dependency(%q<omniauth-facebook>.freeze, ["~> 4.0.0"])
    s.add_development_dependency(%q<omniauth-google-oauth2>.freeze, ["~> 0.5.3"])
    s.add_development_dependency(%q<webmock>.freeze, ["~> 3.3.0"])
    s.add_development_dependency(%q<devise>.freeze, [">= 4.4.3"])
  else
    s.add_dependency(%q<railties>.freeze, [">= 5.0"])
    s.add_dependency(%q<doorkeeper>.freeze, [">= 4.0"])
    s.add_dependency(%q<rspec-rails>.freeze, [">= 2.11.4"])
    s.add_dependency(%q<capybara>.freeze, ["~> 2.18.0"])
    s.add_dependency(%q<factory_bot>.freeze, ["~> 4.8.2"])
    s.add_dependency(%q<generator_spec>.freeze, ["~> 0.9.4"])
    s.add_dependency(%q<database_cleaner>.freeze, ["~> 1.6.2"])
    s.add_dependency(%q<pry>.freeze, [">= 0.11.3"])
    s.add_dependency(%q<appraisal>.freeze, ["~> 2.2.0"])
    s.add_dependency(%q<omniauth-oauth2>.freeze, ["~> 1.5.0"])
    s.add_dependency(%q<omniauth-facebook>.freeze, ["~> 4.0.0"])
    s.add_dependency(%q<omniauth-google-oauth2>.freeze, ["~> 0.5.3"])
    s.add_dependency(%q<webmock>.freeze, ["~> 3.3.0"])
    s.add_dependency(%q<devise>.freeze, [">= 4.4.3"])
  end
end
